﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class LessonPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text selectLessonText;
    [SerializeField]
    private Fix3dTextCS selectLesson_arab;
    [SerializeField]
    private Text ClearDeskText;
    [SerializeField]
    private Fix3dTextCS ClearDeskText_arab;
    [SerializeField]
    private Text Lesson2Text;
    [SerializeField]
    private Fix3dTextCS Lesson2Text_arab;
    [SerializeField]
    private Text Lesson3Text;
    [SerializeField]
    private Fix3dTextCS Lesson3Text_arab;
    


    [SerializeField]
    private Button ClearDeskButton;
    //[SerializeField]
    //private Button evaluationButton;

    [SerializeField]
    private Button langButton;
    [SerializeField]
    private Button homeButton;

    public Content selectLesson;
    public Content ClearDesk;
    public Content Lesson2;
    public Content Lesson3;
   // public Content evaluationDesscription;

    public EventHandler OnClickedClearDeskButton;
   // public EventHandler OnClickedEvaluationButton;
    public EventHandler OnClickedHomeButton;
    public EventHandler OnClickedLangButton;


    private void Awake()
    {
        ClearDeskButton.onClick.AddListener(OnClickedClearDesk);
       // evaluationButton.onClick.AddListener(OnClickedEvaluation);

        langButton.onClick.AddListener(OnClickedLang);
        homeButton.onClick.AddListener(OnClickedHome);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    private void OnClickedClearDesk()
    {
        OnClickedClearDeskButton?.Invoke(this, new EventArgs());
    }

    

    private void OnClickedLang()
    {
        OnClickedLangButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedHome()
    {
        OnClickedHomeButton?.Invoke(this, new EventArgs());
    }


    public void UpdateContentLanguage(Languages language)
    {
        if (language == Languages.English)
        {
            // selectLesson_arab.text = selectLesson.english;
            selectLessonText.gameObject.GetComponent<Fix3dTextCS>().text = selectLesson.english;
            ClearDeskText.gameObject.GetComponent<Fix3dTextCS>().text = ClearDesk.english;
            Lesson2Text_arab.text = Lesson2.english;
            Lesson3Text_arab.text = Lesson3.english;
           // evaluationDescriptionText_arab.text = evaluationDesscription.english;
        }
        else
        {
            selectLessonText.gameObject.GetComponent<Fix3dTextCS>().text = selectLesson.arabic;
            ClearDeskText.gameObject.GetComponent<Fix3dTextCS>().text = ClearDesk.arabic;
            Lesson2Text_arab.text = Lesson2.arabic;
            Lesson3Text_arab.text = Lesson3.arabic;
           // evaluationDescriptionText_arab.text = evaluationDesscription.arabic;
        }

    }

    
}

   
