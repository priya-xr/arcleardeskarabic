using UnityEngine;
using System.Collections;
using ArabicSupport;
using UnityEngine.UI;

public class Fix3dTextCS : MonoBehaviour {
    [HideInInspector]
    public LanguageManager languageManager;

    public string text;
	public bool tashkeel = true;
	public bool hinduNumbers;
  //  private Languages currentLanguage;
    Text t;
   
    // Use this for initialization
    void Start () {
        languageManager = LanguageManager.instance;
        t = gameObject.GetComponent<Text>();
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        // gameObject.GetComponent<TextMesh>().text = ArabicFixer.Fix(text, tashkeel, hinduNumbers);
      //  if (currentLanguage == Languages.Arabic)
      if (languageManager.CurrentLaguage == Languages.Arabic)
        {

            t.text = ArabicFixer.Fix(text, tashkeel, hinduNumbers);
        }
        else
        {
            t.text = text;
        }
        
           
    }
}
