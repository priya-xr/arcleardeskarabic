﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HighlightPlus;


public class ItemOutline : MonoBehaviour
{
    [SerializeField]
    private HighlightEffect[] highlights;
    private float minWidth = 0;
    private float maxWidth = 1f;
    [SerializeField]
    private AnimationCurve animationCurve;

    private float currentTime;
    private bool canBreathe = true;

    private void Awake()
    {
        for (int i = 0; i < highlights.Length; i++)
        {
            highlights[i].glowAlwaysOnTop = true;
        }
        Hide();

    }

    private void Update()
    {
        if (!canBreathe)
            return;

        if (currentTime >= 1f)
        {
            currentTime = 0;
        }
        else
        {
            currentTime += Time.deltaTime;
        }
        UpdateHighlight(currentTime);
    }

    private void UpdateHighlight(float time)
    {
        float t = animationCurve.Evaluate(time);
        t = maxWidth * t;

        for (int i = 0; i < highlights.Length; i++)
        {
            highlights[i].glow = Mathf.Clamp(t, minWidth, maxWidth);
        }
    }

    public void Show()
    {
        for (int i = 0; i < highlights.Length; i++)
        {
            highlights[i].enabled = true;
        }
        canBreathe = true;
    }

    public void Hide()
    {
        for (int i = 0; i < highlights.Length; i++)
        {
            highlights[i].enabled = false;
        }
        canBreathe = false;
    }
}
