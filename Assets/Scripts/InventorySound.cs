﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySound : MonoBehaviour
{
    [SerializeField]
    private Inventory inventory;


    private void Awake()
    {
        inventory.OnItemAdded += OnItemAdded;
        inventory.OnItemRemoved += OnItemRemoved;
    }

    private void OnItemAdded(object sender, Inventory.OnItemAddedEventArgs args)
    {
        AudioManager.instance.PlaySound("PickSound", false);
    }

    private void OnItemRemoved(object sender, Inventory.OnItemRemovedEventArgs args)
    {
        AudioManager.instance.PlaySound("DropSound", false);
    }
}
