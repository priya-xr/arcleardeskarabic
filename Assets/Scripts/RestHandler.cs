﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class RestHandler : MonoBehaviour
{
    public static RestHandler instance;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public void Get(string url, string username, string password, Action<RestGetCallBack> callback)
    {
        StartCoroutine(HandleGet(url, username, password, callback));
    }

    public void Put()
    {

    }

    public void Post(string url, string postString, string username, string password, Action<RestGetCallBack> callback)
    {
        StartCoroutine(HandlePost(url, postString, username, password, callback));
    }

    public void Delete()
    {

    }

    private IEnumerator HandleGet(string url, string username, string password, Action<RestGetCallBack> callback)
    {
        string authorization = Authenticate(username, password);

        UnityWebRequest www = UnityWebRequest.Get(url);
        www.SetRequestHeader("AUTHORIZATION", authorization);

        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            //error 
            Debug.Log("Error =" + www.error);
            callback.Invoke(new RestGetCallBack { commandSuccessfullyCompleted = false });
        }
        else
        {
            if (www.isDone)
            {
                //completed
                callback.Invoke(new RestGetCallBack { commandSuccessfullyCompleted = true, data = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data) });
            }
        }
    }

    private IEnumerator HandlePost(string url, string postString, string username, string password, Action<RestGetCallBack> callback)
    {
        string authorization = Authenticate(username, password);

        UnityWebRequest www = UnityWebRequest.Post(url, postString);
        www.SetRequestHeader("AUTHORIZATION", authorization);

        www.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(postString));
        www.uploadHandler.contentType = "application/json";

        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            //error 
            Debug.Log("Error =" + www.error);
            callback.Invoke(new RestGetCallBack { commandSuccessfullyCompleted = false });
        }
        else
        {
            if (www.isDone)
            {
                //completed
                callback.Invoke(new RestGetCallBack { commandSuccessfullyCompleted = true, data = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data) });
            }
        }
    }


    private string Authenticate(string username, string password)
    {
        string auth = username + ":" + password;
        auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
        auth = "Basic " + auth;
        return auth;
    }
}


[System.Serializable]
public class RestGetCallBack
{
    public bool commandSuccessfullyCompleted;
    public string data;
}