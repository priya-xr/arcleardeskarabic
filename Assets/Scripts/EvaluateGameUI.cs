﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class EvaluateGameUI : MiniGameUI
{

    [SerializeField]
    private EvaluateStartPanel startPanel;
    [SerializeField]
    private EvaluationPanel taskPanel;
    [SerializeField]
    private EvaluateFinishPanel finishPanel;
    [SerializeField]
    private EvaluateScorePanel scorePanel;
    [SerializeField]
    private EvaluateinfoPanel taskInfoPanel;
    [SerializeField]
    private CalibrationPanel calibrationPanel;
    [SerializeField]
    private MainMenuPanel mainMenuPanel;

    private EvaluateGame evaluateGame;
    private LanguageManager languageManager;

    [SerializeField]
    private GameObject normalCanvas;
    [SerializeField]
    private GameObject calibrationCanvas;


    private void Awake()
    {
        evaluateGame = miniGame as EvaluateGame;
        startPanel.OnClickedStartButton += OnClickedStart;
        taskInfoPanel.OnClickedSubmitButton += OnClickedSubmit;

        finishPanel.OnClickedSubmitButton += OnClickedConfirmSubmit;
        finishPanel.OnClickedCancelButton += OnClickedMenuCancel;

        scorePanel.OnClickedSaveAndExitButton += OnClickedSaveAndExit;

        calibrationPanel.OnClickedPlaceButton += OnClickedCalibrationPlace;
        calibrationPanel.OnClickedHomeButton += OnClickedExitToHome;
        calibrationPanel.OnClickedLanguageButton += OnClickedLanguage;


        mainMenuPanel.OnClickedCancelButton += OnClickedMaineMenuCancel;
        mainMenuPanel.OnClickedExitToHomeButton += OnClickedExitToHome;
        mainMenuPanel.OnClickedReScanButton += OnClickedReScan;
        mainMenuPanel.OnClickedArabicButton += OnClickedArabic;
        mainMenuPanel.OnClickedEnglishButton += OnClickedEnglish;

        startPanel.OnClickedMainMenuButton += OnClickedMainMenu;
        taskPanel.OnClickedMainMenuButton += OnClickedMainMenu;
        finishPanel.OnClickedMainMenuButton += OnClickedMainMenu;
        scorePanel.OnClickedMainMenuButton += OnClickedMainMenu;
    }

    private void Start()
    {
        languageManager = evaluateGame.languageManager;
        languageManager.OnLanguageChanged += OnLanguageChanged;
        evaluateGame.sessionManager.OnConnectionStatusChanged += OnConnectionStatusChanged;

        OnLanguageChanged(this, new LanguageManager.OnLanguageChangedArgs { currentLaguage = languageManager.CurrentLaguage });
    }

    public override void ShowPanel(string panelName, bool hideExisting)
    {
        if (hideExisting)
        {
            for (int i = 0; i < uiPanels.Length; i++)
            {
                uiPanels[i].Hide();
            }
        }

        UIPanel uiPanel = GetUiPanel(panelName);
        if (uiPanel != null)
        {
            uiPanel.Show();
        }
    }

    public override void HidePanel(string panelName)
    {
        UIPanel uiPanel = GetUiPanel(panelName);
        if (uiPanel != null)
        {
            uiPanel.Hide();
        }
    }


    protected override UIPanel GetUiPanel(string panelName)
    {
        for (int i = 0; i < uiPanels.Length; i++)
        {
            if (panelName.ToLower() == uiPanels[i].Name.ToLower())
            {
                return uiPanels[i];
            }
        }
        return null;
    }

    public override void UpdatePanelDetails(string panelName, object[] args)
    {
        UIPanel uiPanel = GetUiPanel(panelName);
        if (uiPanel != null)
        {
            uiPanel.UpdateUIPanel(args);
        }
    }

    private void OnClickedStart(object sender, EventArgs args)
    {
        evaluateGame.EnterGame();
    }

    private void OnClickedSubmit(object sender, EventArgs args)
    {
        evaluateGame.PauseGame();
        ShowPanel("FinishPanel", true);
    }

    private void OnClickedConfirmSubmit(object sender, EventArgs args)
    {
        evaluateGame.FinishGame();
    }

    private void OnClickedMenuCancel(object sender, EventArgs args)
    {
        ShowPanel("TaskPanel", true);
        evaluateGame.ResumeGame();
    }

    private void OnClickedSaveAndExit(object sender, EventArgs args)
    {
        evaluateGame.SaveDetailsToServerAndExit();
    }

    public void UpdateTaskSubPanel(string subPanelName, object[] args)
    {
        taskPanel.UpdateUISubPanel(subPanelName, args);
    }

    private void OnLanguageChanged(object sender, LanguageManager.OnLanguageChangedArgs args)
    {
        for (int i = 0; i < uiPanels.Length; i++)
        {
            if (uiPanels[i] == null)
                return;

            ILanguage ILanguage = uiPanels[i].GetComponent<ILanguage>();

            if (ILanguage != null)
            {
                ILanguage.UpdateContentLanguage(args.currentLaguage);
            }
        }
    }

    public void OnClickedMaineMenuCancel(object sender, EventArgs e)
    {
        HidePanel("MainMenuPanel");
        evaluateGame.ResumeGame();
    }

    public void OnClickedMainMenu(object sender, EventArgs e)
    {
        ShowPanel("MainMenuPanel", false);
        evaluateGame.PauseGame();
    }

    public void OnClickedExitToHome(object sender, EventArgs e)
    {
        //load start scene
        evaluateGame.GoHome();
    }

    public void OnClickedArabic(object sender, EventArgs e)
    {
        evaluateGame.ChangeLanguage(Languages.Arabic);
    }

    public void OnClickedEnglish(object sender, EventArgs e)
    {
        evaluateGame.ChangeLanguage(Languages.English);
    }

    public void OnClickedReScan(object sender, EventArgs e)
    {
        ShowPanel("CalibrationPanel", false);
        EnableCalibrationCanvas(true);
        evaluateGame.StartCalibration();
    }

    public void EnableCalibrationCanvas(bool active)
    {
        normalCanvas.SetActive(!active);
        calibrationCanvas.SetActive(active);
    }

    private void OnClickedCalibrationPlace(object sender, EventArgs e)
    {
        evaluateGame.Place();
    }

    private void OnClickedLanguage(object sender, EventArgs e)
    {
        evaluateGame.ToggleLanguage();
    }

    private void OnConnectionStatusChanged(object sender, SessionManager.OnConnectionStatusChangedArgs args)
    {
        if (!args.connected)
        {
            ShowPanel("ConnnectionFailedPanel", false);
        }
    }

}

