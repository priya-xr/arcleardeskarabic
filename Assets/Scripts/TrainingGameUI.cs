﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class TrainingGameUI : MiniGameUI
{
    private TrainingGame trainingGame;
    private LanguageManager languageManager;

    [SerializeField]
    private ModuleStartPanel moduleStartPanel;
    [SerializeField]
    private ModuleFinishPanel moduleFinishPanel;
    [SerializeField]
    private TaskPanel taskPanel;
    [SerializeField]
    private CalibrationPanel calibrationPanel;
    [SerializeField]
    private TrainingScorePanel trainingScorePanel;
    [SerializeField]
    private SubModuleFinishtPanel subModuleFinishtPanel;
    [SerializeField]
    private SubModuleStartPanel subModuleStartPanel;
    [SerializeField]
    private MainMenuPanel mainMenuPanel;

    [SerializeField]
    private GameObject normalCanvas;
    [SerializeField]
    private GameObject calibrationCanvas;

    public TrainingGame TrainingGame { get => trainingGame; }

    private void Awake()
    {
        trainingGame = MiniGame as TrainingGame;

        moduleStartPanel.OnClickedStartButton += OnClickedModuleStart;
        moduleStartPanel.OnClickedSkipButton += OnClickedSkip;

        moduleFinishPanel.OnClickedProceedButton += OnClickedProceed;
        moduleFinishPanel.OnClickedRedoButton += OnClickedRedo;
        moduleFinishPanel.OnClickedSubmitButton += OnClickedSubmit;

        trainingScorePanel.OnClickedSaveAndExitButton += OnClickedSaveAndExit;

        calibrationPanel.OnClickedPlaceButton += OnClickedCalibrationPlace;
        calibrationPanel.OnClickedHomeButton += OnClickeGoHome;
        calibrationPanel.OnClickedLanguageButton += OnClickedLanguage;

        mainMenuPanel.OnClickedCancelButton += OnClickedCancel;
        mainMenuPanel.OnClickedExitToHomeButton += OnClickedExitToHome;
        mainMenuPanel.OnClickedReScanButton += OnClickedReScan;
        mainMenuPanel.OnClickedArabicButton += OnClickedArabic;
        mainMenuPanel.OnClickedEnglishButton += OnClickedEnglish;

        moduleStartPanel.OnClickedMainMenuButton += OnClickedMainMenu;
        moduleFinishPanel.OnClickedMainMenuButton += OnClickedMainMenu;
        taskPanel.OnClickedMainMenuButton += OnClickedMainMenu;
        // taskPanel.OnClickedFixItButton += OnClickedFixIt;
        trainingScorePanel.OnClickedMainMenuButton += OnClickedMainMenu;

        subModuleStartPanel.OnClickedMainMenuButton += OnClickedMainMenu;
        subModuleStartPanel.OnClickedStartButton += OnClickedStartSubTaskButton;

        subModuleFinishtPanel.OnClickedMainMenuButton += OnClickedMainMenu;
        subModuleFinishtPanel.OnClickedNextButton += OnClickedSubTaskNextButton;
    }

    private void Start()
    {
        languageManager = trainingGame.languageManager;
        languageManager.OnLanguageChanged += OnLanguageChanged;
        TrainingGame.sessionManager.OnConnectionStatusChanged += OnConnectionStatusChanged;

        OnLanguageChanged(this, new LanguageManager.OnLanguageChangedArgs { currentLaguage = languageManager.CurrentLaguage });
    }

    public override void ShowPanel(string panelName, bool hideExisting)
    {
        if (hideExisting)
        {
            for (int i = 0; i < uiPanels.Length; i++)
            {
                uiPanels[i].Hide();
            }
        }

        UIPanel uiPanel = GetUiPanel(panelName);
        if (uiPanel != null)
        {
            uiPanel.Show();
        }
    }

    public override void HidePanel(string panelName)
    {
        UIPanel uiPanel = GetUiPanel(panelName);
        if (uiPanel != null)
        {
            uiPanel.Hide();
        }
    }


    protected override UIPanel GetUiPanel(string panelName)
    {
        for (int i = 0; i < uiPanels.Length; i++)
        {
            if (panelName.ToLower() == uiPanels[i].Name.ToLower())
            {
                return uiPanels[i];
            }
        }
        return null;
    }

    public override void UpdatePanelDetails(string panelName, object[] args)
    {
        UIPanel uiPanel = GetUiPanel(panelName);
        if (uiPanel != null)
        {
            uiPanel.UpdateUIPanel(args);
        }
    }

    private void OnClickedModuleStart(object sender, EventArgs e)
    {
        TrainingGame.StartModule(TrainingGame.CurrentModuleIndex);
    }

    private void OnClickedSkip(object sender, EventArgs e)
    {
        TrainingGame.SkipCurrentModule();
    }

    private void OnClickedProceed(object sender, EventArgs e)
    {
        TrainingGame.InitModule(TrainingGame.CurrentModuleIndex + 1);
    }

    private void OnClickedRedo(object sender, EventArgs e)
    {
        TrainingGame.RedoCurrentModule();
    }

    private void OnClickedSubmit(object sender, EventArgs e)
    {
        TrainingGame.FinishGame();
    }

    private void OnClickedSaveAndExit(object sender, EventArgs e)
    {
        TrainingGame.SaveDetailsToServer();
        trainingGame.ExitToModuleSelection();
    }

    private void OnClickedLanguage(object sender, EventArgs e)
    {
        TrainingGame.ToggleLanguage();
    }

    private void OnClickedCalibrationPlace(object sender, EventArgs e)
    {
        trainingGame.Place();
    }

    public void UpdateTaskSubPanel(string subPanelName, object[] args)
    {
        taskPanel.UpdateUISubPanel(subPanelName, args);
    }

    public void ShowTaskSubPanel(string subPanelName)
    {
        taskPanel.ShowSubPanel(subPanelName);
    }

    public void HideTaskSubPanel(string subPanelName)
    {
        taskPanel.HideSubPanel(subPanelName);
    }

    public void OnClickedCancel(object sender, EventArgs e)
    {
        trainingGame.ResumeGame();
        HidePanel("MainMenuPanel");
    }
    public void OnClickedMainMenu(object sender, EventArgs e)
    {
        trainingGame.PauseGame();
        ShowPanel("MainMenuPanel", false);
    }
    //public void OnClickedFixIt(object sender, EventArgs e)
    //{

    //}


    public void OnClickedExitToHome(object sender, EventArgs e)
    {
        //load start scene
        trainingGame.SaveGoHome();
    }

    public void OnClickeGoHome(object sender, EventArgs e)
    {
        //load start scene
        trainingGame.GoHome();
    }

    public void OnClickedArabic(object sender, EventArgs e)
    {
        trainingGame.ChangeLanguage(Languages.Arabic);
    }

    public void OnClickedEnglish(object sender, EventArgs e)
    {
        trainingGame.ChangeLanguage(Languages.English);
    }

    public void OnClickedReScan(object sender, EventArgs e)
    {
        ShowPanel("CalibrationPanel", false);
        EnableCalibrationCanvas(true);
        trainingGame.StartCalibration();
    }

    private void OnLanguageChanged(object sender, LanguageManager.OnLanguageChangedArgs args)
    {
        for (int i = 0; i < uiPanels.Length; i++)
        {
            if (uiPanels[i] == null)
                return;

            ILanguage ILanguage = uiPanels[i].GetComponent<ILanguage>();

            if (ILanguage != null)
            {
                ILanguage.UpdateContentLanguage(args.currentLaguage);
            }
        }
    }

    public void EnableCalibrationCanvas(bool active)
    {
        normalCanvas.SetActive(!active);
        calibrationCanvas.SetActive(active);
    }

    private void OnConnectionStatusChanged(object sender, SessionManager.OnConnectionStatusChangedArgs args)
    {
        if (!args.connected)
        {
            ShowPanel("ConnnectionFailedPanel", false);
        }
    }

    public void OnClickedStartSubTaskButton(object sender, EventArgs e)
    {
        trainingGame.StartSubTask();
    }

    public void OnClickedSubTaskNextButton(object sender, EventArgs e)
    {
        trainingGame.SubModuleFinishPanelConfirmation();
    }

}

