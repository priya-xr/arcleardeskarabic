﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class BaseGame : MiniGame
{
    [SerializeField]
    private MiniGameUI miniGameUI;
    private BaseGameUI baseGameUI;

    [HideInInspector]
    public LanguageManager languageManager;
    [HideInInspector]
    public LevelManager levelManager;
    [HideInInspector]
    public SessionManager sessionManager;

    private void Awake()
    {
        baseGameUI = miniGameUI as BaseGameUI;
        languageManager = LanguageManager.instance;
        levelManager = LevelManager.instance;
        sessionManager = SessionManager.instance;
    }

    private void Start()
    {
        Application.targetFrameRate = 60;
        EnterGame();
    }

    public override void EnterGame()
    {
        if (GlobalValues.instance.BaseStates == BASESTATES.BEGIN)
        {
            StartCoroutine(ShowTitle());
        }
        else if (GlobalValues.instance.BaseStates == BASESTATES.MODULESELECTION)
        {
            string evaluationState = sessionManager.IsTrainingComplete().ToString().ToLower();
            baseGameUI.UpdatePanelDetails("ModuleSelectPanel", new object[] { evaluationState });
            baseGameUI.ShowPanel("ModuleSelectPanel", true);
        }
    }

    private IEnumerator ShowTitle()
    {
        baseGameUI.ShowPanel("TitlePanel", true);
        yield return new WaitForSeconds(2f);
        baseGameUI.ShowPanel("LanguageSelectPanel", true);
    }

    public override void UpdateGame()
    {

    }

    public override void FinishGame()
    {

    }

    public override void ExitGame()
    {

    }

    public override void ResetGame()
    {

    }

    public void CreateSession(int employeeID,string platform, Action OnSessionCreated)
    {
        sessionManager.CreateSession(employeeID,platform, () => { OnSessionCreated?.Invoke(); });
    }

    public void SetLanguage(Languages language)
    {
        languageManager.ChangeLanguage(language);
    }

    public void ToggleLanguage()
    {
        languageManager.ToggleLanguage();
    }

    public void LoadLevel(string levelName)
    {
        levelManager.LoadScene(levelName);
    }
}
