﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalValues : MonoBehaviour
{
    public static GlobalValues instance;
    [SerializeField]
    private TRAININGSTATES trainingState;
    [SerializeField]
    private BASESTATES baseStates;

    public TRAININGSTATES TrainingState { get => trainingState; set => trainingState = value; }
    public BASESTATES BaseStates { get => baseStates; set => baseStates = value; }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
}


public enum TRAININGSTATES
{
    RESUME,
    NEWSESSION
}
public enum BASESTATES
{
    BEGIN,
    MODULESELECTION
}