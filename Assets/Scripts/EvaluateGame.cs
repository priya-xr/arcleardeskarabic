﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EvaluateGame : Game
{
    [SerializeField]
    private MiniGameUI gameUI;
    private LevelManager levelManager;
    [HideInInspector]
    public SessionManager sessionManager;

    [SerializeField]
    private EvaluateModule[] allModules;

    [SerializeField]
    private List<EvaluateModuleTask> shuffledTasks = new List<EvaluateModuleTask>();
    [SerializeField]
    private List<Task> noScoreTasks = new List<Task>();


    [SerializeField]
    private EvaluateModuleTask tableTask;
    [SerializeField]
    private EvaluateModuleTask lockTableTask;
    [SerializeField]
    private EvaluateModuleTask keepKeyInHandBagTask;

    [SerializeField]
    private Timer timer;
    private bool isGameStarted;

    public GameObject pinch;
    public GameObject zoom;

    private EvaluateGameUI evaluateGameUI;
    private float gameTime = 90;//in secs



    [Header("AR")]
    [SerializeField]

    private ARFoundationCalibration aRFoundationCalibration;
    [SerializeField]
    private TouchControl touchControl;
    [SerializeField]
    private Transform arRoot;
    [SerializeField]
    private Transform dummy;
    [SerializeField]
    private Transform actual;
    [SerializeField]
    private LookAt lookAt;


    private void Awake()
    {

    }

    private void Start()
    {
        languageManager = LanguageManager.instance;
        levelManager = LevelManager.instance;

        sessionManager = SessionManager.instance;

        evaluateGameUI = gameUI as EvaluateGameUI;

        evaluateGameUI.EnableCalibrationCanvas(true);
        evaluateGameUI.ShowPanel("CalibrationPanel", false);
        StartCalibration();
        //EnterGame();
    }

    public override void EnterGame()
    {
        isGameStarted = true;

        evaluateGameUI.ShowPanel("TaskPanel", true);

        timer.StartTimer(gameTime);
        timer.OnTimerUpdated += OnTimerUpdated;
        timer.OnTimerFinished += DisplayScore;

        ShuffleTasks();
        CompleteTasks();
        StartAllShuffledTasks();
        StartAllNoScoreTasks();
        base.EnterGame();
    }

    public override void PauseGame()
    {
        timer.PauseTimer();
    }

    public override void ResumeGame()
    {
        timer.ResumeTimer();
    }

    public void StartAllShuffledTasks()
    {
        for (int i = 0; i < shuffledTasks.Count; i++)
        {
            shuffledTasks[i].task.StartTask();
        }
    }
    public void StartAllNoScoreTasks()
    {
        for (int i = 0; i < noScoreTasks.Count; i++)
        {
            noScoreTasks[i].StartTask();
        }
    }


    private void CompleteTasks()
    {
        StartCoroutine(Complete(shuffledTasks));
    }

    private IEnumerator Complete(List<EvaluateModuleTask> tasksToIgnore)
    {
        //yield return new WaitForSeconds(10f);
        //show loading screen
        evaluateGameUI.ShowPanel("LoadingPanel", false);

        for (int i = 0; i < allModules.Length; i++)
        {
            for (int j = 0; j < allModules[i].tasks.Length; j++)
            {
                float t = 0;
                if (!ToIgnore(i, j))
                    t = allModules[i].tasks[j].CompleteTask();

                yield return new WaitForSeconds(t + .1f);
            }
        }

        evaluateGameUI.HidePanel("LoadingPanel");
        //hide loading screen
        yield return null;
    }

    private bool ToIgnore(int moduleIndex, int taskIndex)
    {
        for (int i = 0; i < shuffledTasks.Count; i++)
        {
            if ((shuffledTasks[i].moduleIndex == moduleIndex) && (shuffledTasks[i].taskIndex == taskIndex))
            {
                return true;
            }
        }
        return false;
    }

    public void ToggleLanguage()
    {
        languageManager.ToggleLanguage();
    }

    private void ShuffleTasks()
    {
        shuffledTasks.Clear();

        int randomModuleIndex = 0;
        int randomTaskIndex = 0;
        List<Task> tasks = new List<Task>();

        while (shuffledTasks.Count != 3)
        {
            randomModuleIndex = UnityEngine.Random.Range(0, allModules.Length);
            randomTaskIndex = UnityEngine.Random.Range(0, allModules[randomModuleIndex].tasks.Length);

            EvaluateModuleTask task = new EvaluateModuleTask();
            task.moduleIndex = randomModuleIndex;
            task.task = allModules[randomModuleIndex].tasks[randomTaskIndex];
            task.taskIndex = randomTaskIndex;

            if (!tasks.Contains(task.task))
            {
                shuffledTasks.Add(task);
                tasks.Add(task.task);
            }
        }
        shuffledTasks.Add(tableTask);
        shuffledTasks.Add(lockTableTask);
        shuffledTasks.Add(keepKeyInHandBagTask);
    }

    private void OnTimerUpdated(object sender, double time)
    {
        object[] args = new object[1];
        args[0] = ((this.gameTime - time) / 60).ToString("F2");

        evaluateGameUI.UpdateTaskSubPanel("TaskInfoPanel", args);
    }

    private void DisplayScore(object sender, System.EventArgs args)
    {
        timer.StopTimer();
        evaluateGameUI.ShowPanel("ScorePanel", true);

        //0-employee id
        //1-score
        //2-module 1 score
        //3-module 2 score
        //4-module 3 score

        string employeeID = sessionManager.CurrentSession.employeeID.ToString();
        string totalScore = "";
        string m1Score = "";
        string m2Score = "";
        string m3Score = "";

        GetScore(out totalScore, out m1Score, out m2Score, out m3Score);

        object[] o = new object[5];
        o[0] = employeeID;
        o[1] = totalScore;
        o[2] = m1Score;
        o[3] = m2Score;
        o[4] = m3Score;

        evaluateGameUI.UpdatePanelDetails("ScorePanel", o);

        EvaluationScore evaluationScore = new EvaluationScore(totalScore, m1Score, m2Score, m3Score);
        sessionManager.SetCurrentEvaluationDetails(evaluationScore);
    }

    private void GetScore(out string totalScore, out string m1Score, out string m2Score, out string m3Score)
    {
        totalScore = "0;";
        m1Score = "0;";
        m2Score = "0;";
        m3Score = "0;";

        int totalFinishedTasks = 0;
        for (int i = 0; i < shuffledTasks.Count; i++)
        {
            if (shuffledTasks[i].task.IsTaskFinished)
                totalFinishedTasks++;
        }

        totalScore = (((float)totalFinishedTasks / (float)shuffledTasks.Count) * 100f).ToString("F0") + "%";
        GetModuleScore(0, out m1Score);
        GetModuleScore(1, out m2Score);
        GetModuleScore(2, out m3Score);
    }

    private void GetModuleScore(int moduleIndex, out string score)
    {
        score = "-";
        int totalModuleTasks = 0;
        int completedTasks = 0;

        for (int i = 0; i < shuffledTasks.Count; i++)
        {
            if (moduleIndex == shuffledTasks[i].moduleIndex)
            {
                totalModuleTasks++;
                if (shuffledTasks[i].task.IsTaskFinished)
                {
                    completedTasks++;
                }
            }
        }
        if (totalModuleTasks == 0)
        {
            score = "-";
            return;
        }

        score = (((float)completedTasks / (float)totalModuleTasks) * 100f).ToString("F0") + "%";
    }

    public void SaveDetailsToServerAndExit()
    {
        //save to server here
        sessionManager.CurrentSession.type="Evaluation";
        SessionDetailsPost session = new SessionDetailsPost(sessionManager.CurrentSession);
        string jsonString = JsonUtility.ToJson(session);
        RestHandler.instance.Post(APILinks.postSessionOfEmployee, jsonString, APILinks.userName
                                , APILinks.password, (RestGetCallBack args) => { });


        GlobalValues.instance.BaseStates = BASESTATES.BEGIN;
        levelManager.LoadScene("Start");
    }

    public override void UpdateGame()
    {

    }

    public override void FinishGame()
    {
        DisplayScore(this, new System.EventArgs());
        isGameStarted = false;
        base.FinishGame();
    }

    public override void ExitGame()
    {
        base.ExitGame();
    }

    public override void ResetGame()
    {
        base.ResetGame();
    }

    //----------
    //Calibration
    //----------
    //Calibration
    public void StartCalibration()
    {
        if (isGameStarted)
        {
            PauseGame();
        }

        aRFoundationCalibration.OnPlaneFound += OnPlaneFound;
        aRFoundationCalibration.OnPlaneNotFound += OnPlaneNotFound;
        touchControl.WhenSwiped += RotateARRoot;
        touchControl.WhenPinched += ScaleARRoot;

        dummy.gameObject.SetActive(false);
        actual.gameObject.SetActive(false);

        aRFoundationCalibration.StartCalibration();
        touchControl.enabled = true;
        lookAt.enabled = true;
    }

    private void StopCalibration()
    {
        aRFoundationCalibration.OnPlaneFound -= OnPlaneFound;
        aRFoundationCalibration.OnPlaneNotFound -= OnPlaneNotFound;
        touchControl.WhenSwiped -= RotateARRoot;
        touchControl.WhenPinched -= ScaleARRoot;

        aRFoundationCalibration.StopCalibration();
        touchControl.enabled = false;
        lookAt.enabled = false;
    }

    private void OnPlaneFound(object sender, ARFoundationCalibration.OnPlaneFoundEventArgs args)
    {
        arRoot.position = args.hitPosition;
        //arRoot.rotation = args.hitRotation;
        dummy.gameObject.SetActive(true);
        pinch.SetActive(true);
        zoom.SetActive(true);
        //button.SetActive(true);

        //set intractable of placebtn set to true
        string s = "true";
        evaluateGameUI.UpdatePanelDetails("CalibrationPanel", new object[1] { s });
    }

    private void OnPlaneNotFound(object sender, EventArgs args)
    {
        dummy.gameObject.SetActive(false);
        pinch.SetActive(false);
        zoom.SetActive(false);
        //set intractable of placebtn set to false
        string s = "false";
        evaluateGameUI.UpdatePanelDetails("CalibrationPanel", new object[1] { s });
    }

    public void Place()
    {
        evaluateGameUI.HidePanel("MainMenuPanel");
        dummy.gameObject.SetActive(false);
        pinch.SetActive(false);
        zoom.SetActive(false);
        //button.gameObject.SetActive(false);
        actual.gameObject.SetActive(true);
        StopCalibration();
        if (!isGameStarted)
        {
            //InitModule(0);
            evaluateGameUI.ShowPanel("StartPanel", true);
        }
        else
        {
            // ResumeGame();
        }

        evaluateGameUI.EnableCalibrationCanvas(false);
        evaluateGameUI.HidePanel("CalibrationPanel");
    }

    public void RotateARRoot(Vector3 delta)
    {
        lookAt.offset += delta.x * -.1f;
        //arRoot.transform.Rotate(new Vector3(0, delta.x * -.1f, 0));
    }

    public void ScaleARRoot(float delta)
    {
        float min = .1f;
        float max = .25f;
        float currentScale = arRoot.transform.localScale.x + (delta * .0005f);

        currentScale = Mathf.Clamp(currentScale, min, max);
        arRoot.transform.localScale = Vector3.one * currentScale;
    }

    public void GoHome()
    {
        GlobalValues.instance.BaseStates = BASESTATES.BEGIN;
        levelManager.LoadScene("Start");
    }

    public void ChangeLanguage(Languages language)
    {
        languageManager.ChangeLanguage(language);
    }


}



[System.Serializable]
public class EvaluateModule
{
    public Content moduleID;
    public int moduleIndex;
    public Task[] tasks;
}


[System.Serializable]
public class EvaluateModuleTask
{
    public int moduleIndex;
    public int taskIndex;
    public Task task;
}

