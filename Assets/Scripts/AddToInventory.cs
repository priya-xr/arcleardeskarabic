﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AddToInventory : MonoBehaviour
{

    [SerializeField]
    private Game game;
    [SerializeField]
    private TapItem tapItem;
    [SerializeField]
    private float intractionTime;
    private void Awake()
    {
        tapItem.IntractionTime = intractionTime;
        tapItem.OnTapped += OnTappedItem;
        tapItem.IsIntratable = true;
    }

    private void OnTappedItem(object sender, EventArgs args)
    {
        if (game.Inventory.ItemInHand != null && game.Inventory.ItemInHand != tapItem)
        {
            Debug.Log("Item in Hand"+game.Inventory.ItemInHand);
            game.Inventory.ItemInHand.Show();
        }
        tapItem.Hide();
        game.Inventory.AddItem(tapItem);
    }
}
