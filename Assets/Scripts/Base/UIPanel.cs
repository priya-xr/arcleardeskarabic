﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIPanel : MonoBehaviour
{
    public string Name;

    public virtual void Show()
    {

    }

    public virtual void Hide()
    {

    }

    public virtual void UpdateUIPanel(object[] args)
    {

    }

}
