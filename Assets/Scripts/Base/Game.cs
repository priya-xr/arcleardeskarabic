﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MiniGame
{
    [SerializeField]
    protected Inventory inventory;
    [HideInInspector]
    public LanguageManager languageManager;

    public Inventory Inventory { get => inventory; }

}
