﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    public Content ItemID;
    public Sprite icon;

    public string ItemName;//only for reading

    public void Show()
    {
        gameObject.SetActive(true);
        // Debug.Log("Item visible" + ItemName);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        //Debug.Log("Item InVisible" + ItemName);
    }
}
