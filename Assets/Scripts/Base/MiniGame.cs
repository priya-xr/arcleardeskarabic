﻿using UnityEngine;
using System;
public abstract class MiniGame : MonoBehaviour
{
    public EventHandler OnGameEnter;
    public EventHandler OnGameFinished;
    public EventHandler OnGameExit;
    public EventHandler OnGameReset;


    public virtual void EnterGame()
    {
        OnGameEnter?.Invoke(this, new EventArgs());
    }

    public virtual void UpdateGame()
    {

    }

    public virtual void FinishGame()
    {
        OnGameFinished?.Invoke(this, new EventArgs());
    }

    public virtual void ResumeGame()
    {
        OnGameFinished?.Invoke(this, new EventArgs());
    }

    public virtual void PauseGame()
    {
        OnGameFinished?.Invoke(this, new EventArgs());
    }
    

    public virtual void ExitGame()
    {
        OnGameExit?.Invoke(this, new EventArgs());
    }

    public virtual void ResetGame()
    {
        OnGameReset?.Invoke(this, new EventArgs());
    }
}

