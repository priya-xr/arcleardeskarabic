﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IIntractable
{
    void Intract();
    void CancelIntraction();
}

public interface ISound
{
    void PlayAudio(AudioClip audioClip);
    void StopAudio();
}

public interface IAnimatable
{
    float PlayAnimation(string clipName);
    void StopAnimation();
    void LoadAllAnimations();
}

public interface IState
{
    void SetAttendedState();
    void SetUnAttendedState();
}

public interface IHighlightable
{
    void Highlight();
    void UnHighlight();
}

public interface ILanguage
{
    void UpdateContentLanguage(Languages language);
}