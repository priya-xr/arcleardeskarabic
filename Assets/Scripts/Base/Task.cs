﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public abstract class Task : MonoBehaviour
{
    public EventHandler OnTaskStarted;
    public EventHandler OnTaskFinished;
    public EventHandler OnTaskReset;

    public bool IsTaskFinished;

    public virtual void StartTask()
    {
        OnTaskStarted?.Invoke(this, new EventArgs());
    }

    public virtual void FinishTask()
    {
        IsTaskFinished = true;
        OnTaskFinished?.Invoke(this, new EventArgs());
    }

    public virtual void ResetTask()
    {
        IsTaskFinished = false;
        OnTaskReset?.Invoke(this, new EventArgs());
    }

    public virtual float CompleteTask()
    {
        return 0;
    }
}
