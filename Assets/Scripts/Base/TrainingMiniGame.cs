﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingMiniGame : Game
{
    [SerializeField]
    protected OffScreenIndicator offScreenIndicator;

    public OffScreenIndicator OffScreenIndicator { get => offScreenIndicator; }
}
