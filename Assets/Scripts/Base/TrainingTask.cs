﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public abstract class TrainingTask : Task
{
    [SerializeField]
    private Transform taskTransform;
    [SerializeField]
    private Item taskItem;

    public Transform TaskTransform { get => taskTransform; }
    public Item TaskItem { get => taskItem; }
}
