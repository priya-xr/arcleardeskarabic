﻿using UnityEngine;


public abstract class MiniGameUI : MonoBehaviour
{
    [SerializeField]
    protected MiniGame miniGame;
    [SerializeField]
    protected UIPanel[] uiPanels;

    public MiniGame MiniGame { get => miniGame; }

    public virtual void ShowPanel(string panelName, bool hideExisting)
    {

    }

    public virtual void HidePanel(string panelName)
    {

    }

    protected virtual UIPanel GetUiPanel(string panelName)
    {

        return null;
    }

    public virtual void UpdatePanelDetails(string panelName, object[] args)
    {

    }

}

