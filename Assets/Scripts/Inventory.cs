﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Inventory : MonoBehaviour
{
    [SerializeField]
    private Item itemInHand;

    public class OnItemAddedEventArgs : EventArgs
    {
        public Item addedItem;
    }
    public class OnItemRemovedEventArgs : EventArgs
    {
        public Item removedItem;
    }
    public EventHandler<OnItemAddedEventArgs> OnItemAdded;
    public EventHandler<OnItemRemovedEventArgs> OnItemRemoved;

    public Item ItemInHand { get => itemInHand; }

    private void Awake()
    {

    }

    public void AddItem(Item item)
    {
        itemInHand = item;
        OnItemAdded?.Invoke(this, new OnItemAddedEventArgs
        {
            addedItem = itemInHand
        }
        );
    }

    public void RemoveItem(Item item)
    {
        OnItemRemoved?.Invoke(this, new OnItemRemovedEventArgs
        {
            removedItem = itemInHand
        }
        );

        itemInHand = null;
    }
}
