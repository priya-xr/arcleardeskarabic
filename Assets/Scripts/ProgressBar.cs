﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField]
    private Image image;

    public EventHandler OnStart;
    public EventHandler OnComplete;
    public EventHandler OnStop;

    private bool updateProgress;
    private float targetTime;
    private float currentTime;


    private void Awake()
    {
        targetTime = 0f;
        currentTime = 0f;
        updateProgress = false;
        image.fillAmount = 0;
    }

    private void Update()
    {
        if (updateProgress)
        {
            if (currentTime < targetTime)
            {
                currentTime += Time.deltaTime;
                image.fillAmount = currentTime / targetTime;
            }
            else
            {
                OnComplete?.Invoke(this, new EventArgs());
                StopProgress();
            }
        }
    }

    public void StartProgress(float targetTime)
    {
        this.targetTime = targetTime;
        updateProgress = true;

        OnStart?.Invoke(this, new EventArgs());
    }

    public void StopProgress()
    {
        targetTime = 0f;
        currentTime = 0f;
        updateProgress = false;
        image.fillAmount = 0;

        OnStop?.Invoke(this, new EventArgs());
    }
}
