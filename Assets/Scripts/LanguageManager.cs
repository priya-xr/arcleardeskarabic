﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LanguageManager : MonoBehaviour
{
    public static LanguageManager instance;


    [SerializeField]
    private Languages currentLaguage;
    public Languages CurrentLaguage { get => currentLaguage; }

    public class OnLanguageChangedArgs : EventArgs
    {
        public Languages currentLaguage;
    }
    public EventHandler<OnLanguageChangedArgs> OnLanguageChanged;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
    private void Start()
    {
        currentLaguage = Languages.English;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            ToggleLanguage();
        }
    }

    public void ChangeLanguage(Languages language)
    {
        currentLaguage = language;
        //if (currentLaguage == Languages.English)
        //{
        //    // currentLaguage = Languages.English;
        //    currentLaguage = Languages.Arabic;
        //}
        //else
        //{
        //    currentLaguage = Languages.English;
        //}
        //currentLaguage = Languages.English;
        OnLanguageChanged?.Invoke(this, new OnLanguageChangedArgs { currentLaguage = this.CurrentLaguage });
    }

    public void ToggleLanguage()
    {
        if (currentLaguage == Languages.English)
        {
            // currentLaguage = Languages.English;
            currentLaguage = Languages.Arabic;
        }
        else
        {
            currentLaguage = Languages.English;
        }

        OnLanguageChanged?.Invoke(this, new OnLanguageChangedArgs { currentLaguage = this.CurrentLaguage });
    }
}

public enum Languages
{
    English,
    Arabic
}
