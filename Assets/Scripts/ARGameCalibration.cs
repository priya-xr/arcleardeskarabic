﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ARGameCalibration : MonoBehaviour
{
    public ARFoundationCalibration aRFoundationCalibration;
    public Transform arRoot;
    public Transform dummy;
    public Transform actual;
    public GameObject button;
    public TouchControl touchControl;

    private void Awake()
    {
        aRFoundationCalibration.OnPlaneFound += OnPlaneFound;
        aRFoundationCalibration.OnPlaneNotFound += OnPlaneNotFound;
        touchControl.WhenSwiped += Rotate;

        dummy.gameObject.SetActive(false);
        button.SetActive(false);
        actual.gameObject.SetActive(false);
    }

    private void Start()
    {
        aRFoundationCalibration.StartCalibration();
    }
    
    private void OnPlaneFound(object sender, ARFoundationCalibration.OnPlaneFoundEventArgs args)
    {
        arRoot.position = args.hitPosition;
        //arRoot.rotation = args.hitRotation;

        dummy.gameObject.SetActive(true);
        button.SetActive(true);
    }

    private void OnPlaneNotFound(object sender, EventArgs args)
    {
        dummy.gameObject.SetActive(false);
        button.SetActive(false);
    }

    public void Place()
    {
        dummy.gameObject.SetActive(false);
        button.gameObject.SetActive(false);
        actual.gameObject.SetActive(true);

        aRFoundationCalibration.StopCalibration();
        touchControl.enabled = false;
    }
    public void Rotate(Vector3 delta)
    {
        arRoot.transform.Rotate(new Vector3(0, delta.x * .1f, 0));
    }

    public bool show;
    public void ShowMesh()
    {
        show = !show;

        aRFoundationCalibration.SetAllPlanesActive(show);
    }
}
