﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionFailedPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Button retryButton;

    [SerializeField]
    private Text noInternetText;
    [SerializeField]
    private Fix3dTextCS noInternetText_arab;
    [SerializeField]
    private Text infoText;
    [SerializeField]
    private Fix3dTextCS infoText_arab;
    [SerializeField]
    private Text retryButtonText;
    [SerializeField]
    private Fix3dTextCS retryButtonText_arab;

    public Content noInternetContent;
    public Content infotContent;
    public Content retryContent;

    public EventHandler OnClickedRetryButton;


    private void Awake()
    {
        retryButton.onClick.AddListener(OnClickedRetry);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    private void OnClickedRetry()
    {
        OnClickedRetryButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        if (language == Languages.English)
        {
            noInternetText_arab.text = noInternetContent.english;
            infoText_arab.text = infotContent.english;
            retryButtonText_arab.text = retryContent.english;
        }
        else if (language == Languages.Arabic)
        {
            noInternetText_arab.text = noInternetContent.arabic;
            infoText_arab.text = infotContent.arabic;
            retryButtonText_arab.text = retryContent.arabic;
        }
    }
}
