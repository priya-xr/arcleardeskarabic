﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ResumePanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text resumeText;
    [SerializeField]
    private Fix3dTextCS resumeText_arab;
    [SerializeField]
    private Text resumeInfoText;
    [SerializeField]
    private Fix3dTextCS resumeInfoText_arab;


    [SerializeField]
    private Button resumeButton;
    [SerializeField]
    private Button newSessionButton;
    [SerializeField]
    private Button homeButton;
    [SerializeField]
    private Button languageSwapButton;

    [SerializeField]
    private Text resumeButtonText;
    [SerializeField]
    private Fix3dTextCS resumeButtonText_arab;
    [SerializeField]
    private Text newSessionButtonText;
    [SerializeField]
    private Fix3dTextCS newSessionButtonText_arab;

    [Header("Static Content")]
    public Content resumeContent;
    public Content resumeInfoContent;
    public Content resumeButtonContent;
    public Content newSessionButtonContent;

    public EventHandler OnClickedResumeButton;
    public EventHandler OnClickedNewSessionButton;
    public EventHandler OnClickedHomeButton;
    public EventHandler OnClickedLanguageSwapButton;


    private void Awake()
    {
        resumeButton.onClick.AddListener(OnClickedResume);
        newSessionButton.onClick.AddListener(OnClickedNewSession);
        homeButton.onClick.AddListener(OnClickedHome);
        languageSwapButton.onClick.AddListener(OnClickedSwapLanguage);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        //0-new session enable
        string enable = args[0] as string;
        enable=enable.ToLower();

        if (enable.Equals("true"))
        {
            newSessionButton.interactable = true;
        }
        else
        {
            newSessionButton.interactable = false;
        }
    }

    private void OnClickedResume()
    {
        OnClickedResumeButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedNewSession()
    {
        OnClickedNewSessionButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedHome()
    {
        OnClickedHomeButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedSwapLanguage()
    {
        OnClickedLanguageSwapButton?.Invoke(this, new EventArgs());
    }


    public void UpdateContentLanguage(Languages language)
    {

        if (language == Languages.English)
        {
            //static 
            resumeText_arab.text = resumeContent.english;
            resumeInfoText_arab.text = resumeInfoContent.english;
            resumeButtonText_arab.text = resumeButtonContent.english;
            newSessionButtonText_arab.text = newSessionButtonContent.english;
        }
        else
        {
            //static 
            resumeText_arab.text = resumeContent.arabic;
            resumeInfoText_arab.text = resumeInfoContent.arabic;
            resumeButtonText_arab.text = resumeButtonContent.arabic;
            newSessionButtonText_arab.text = newSessionButtonContent.arabic;
        }

    }
}
