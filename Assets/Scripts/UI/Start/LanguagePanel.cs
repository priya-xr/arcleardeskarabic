﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LanguagePanel : UIPanel
{
    [SerializeField]
    private Button englishButton;
    [SerializeField]
    private Button arabicButton;

    public EventHandler OnClickedEnglishButton;
    public EventHandler OnClickedArabicButton;


    private void Awake()
    {
        englishButton.onClick.AddListener(OnClickedEnglish);
        arabicButton.onClick.AddListener(OnClickedArabic);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    private void OnClickedEnglish()
    {
        OnClickedEnglishButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedArabic()
    {
        OnClickedArabicButton?.Invoke(this, new EventArgs());
    }
}
