﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ModuleSelectPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text selectTrainingText;
    [SerializeField]
    private Fix3dTextCS selectTrainingText_arab;
    [SerializeField]
    private Text trainingText;
    [SerializeField]
    private Fix3dTextCS trainingText_arab;
    [SerializeField]
    private Text trainingDescriptionText;
    [SerializeField]
    private Fix3dTextCS trainingDescriptionText_arab;
    [SerializeField]
    private Text evaluationText;
    [SerializeField]
    private Fix3dTextCS evaluationText_arab;
    [SerializeField]
    private Text evaluationDescriptionText;
    [SerializeField]
    private Fix3dTextCS evaluationDescriptionText_arab;
    [SerializeField]
    private Text evaluationTextOrg;
    [SerializeField]
    private Text evaluationDescriptionTextOrg;


    [SerializeField]
    private Button trainingButton;
    [SerializeField]
    private Button evaluationButton;

    [SerializeField]
    private Button langButton;
    [SerializeField]
    private Button homeButton;

    public Content selectTraining;
    public Content training;
    public Content trainingDesscription;
    public Content evaluation;
    public Content evaluationDesscription;

    public EventHandler OnClickedTrainingButton;
    public EventHandler OnClickedEvaluationButton;
    public EventHandler OnClickedHomeButton;
    public EventHandler OnClickedLangButton;


    private void Awake()
    {
        trainingButton.onClick.AddListener(OnClickedTraining);
        evaluationButton.onClick.AddListener(OnClickedEvaluation);

        langButton.onClick.AddListener(OnClickedLang);
        homeButton.onClick.AddListener(OnClickedHome);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        string evluationState = args[0] as string;

        if (evluationState.Equals("true"))
        {
            EnableEvaluationButton();
        }
        else
        {
            DisableEvaluationButton();
        }
    }

    private void OnClickedTraining()
    {
        OnClickedTrainingButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedEvaluation()
    {
        OnClickedEvaluationButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedLang()
    {
        OnClickedLangButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedHome()
    {
        OnClickedHomeButton?.Invoke(this, new EventArgs());
    }


    public void UpdateContentLanguage(Languages language)
    {
        if (language == Languages.English)
        {
            selectTrainingText_arab.text = selectTraining.english;
            trainingText_arab.text = training.english;
            trainingDescriptionText_arab.text = trainingDesscription.english;
            evaluationText_arab.text = evaluation.english;
            evaluationDescriptionText_arab.text = evaluationDesscription.english;
        }
        else
        {
            selectTrainingText_arab.text = selectTraining.arabic;
            trainingText_arab.text = training.arabic;
            trainingDescriptionText_arab.text = trainingDesscription.arabic;
            evaluationText_arab.text = evaluation.arabic;
            evaluationDescriptionText_arab.text = evaluationDesscription.arabic;
        }

    }

    private void EnableEvaluationButton()
    {
        evaluationButton.interactable = true;
        evaluationDescriptionTextOrg.color = new Color(evaluationDescriptionTextOrg.color.r,
                                                    evaluationDescriptionTextOrg.color.g,
                                                    evaluationDescriptionTextOrg.color.b, 1f);

        evaluationTextOrg.color = evaluationTextOrg.color = new Color(evaluationTextOrg.color.r,
                                                    evaluationTextOrg.color.g,
                                                    evaluationTextOrg.color.b, 1f);
    }

    private void DisableEvaluationButton()
    {
        evaluationButton.interactable = false;
        evaluationDescriptionTextOrg.color = new Color(evaluationDescriptionTextOrg.color.r,
                                                    evaluationDescriptionTextOrg.color.g,
                                                    evaluationDescriptionTextOrg.color.b, .5f);

        evaluationTextOrg.color = evaluationTextOrg.color = new Color(evaluationTextOrg.color.r,
                                                    evaluationTextOrg.color.g,
                                                    evaluationTextOrg.color.b, .5f);
    }
}
