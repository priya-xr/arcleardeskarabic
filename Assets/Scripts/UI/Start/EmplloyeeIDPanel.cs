﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class EmplloyeeIDPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text enterEmployeeIDText;
    [SerializeField]
    private Fix3dTextCS enterEmployeeIDText_arab;
    // private Text enterEmployeeIDText;
    [SerializeField]
    private Text employeeIDInfoText;
    [SerializeField]
    private Fix3dTextCS employeeIDInfoText_arab;
    [SerializeField]
    private Text continueButtonText;
    [SerializeField]
    private Fix3dTextCS continueButtonText_arab;
    [SerializeField]
    private Text inputFieldText;
    [SerializeField]
    private Fix3dTextCS inputFieldText_arab;
    [SerializeField]
    private Text incorrectText;
    [SerializeField]
    private Fix3dTextCS incorrectText_arab;

    [SerializeField]
    private Button continueButton;
    [SerializeField]
    private Button langguageButton;
    [SerializeField]
    private InputField inputField;
    [SerializeField]
    private InputField inputField_arab;


    private int typedId;
    private Languages lang;

    public Content enterEmployeeID;
    public Content employeeIDInfo;
    public Content continueContent;
    public Content inputFieldContent;
    public Content incorrectContent;


    public class OnClickedContinueButtonEventArgs : EventArgs
    {
        public int employeeID;
    }
    public EventHandler<OnClickedContinueButtonEventArgs> OnClickedContinueButton;
    public EventHandler OnClickedLanguageButton;


    private void Awake()
    {
        continueButton.onClick.AddListener(OnClickedContinue);
        langguageButton.onClick.AddListener(OnClickedLanguage);
        incorrectText.gameObject.SetActive(false);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    private void OnClickedContinue()
    {
        typedId = 0;

        if(lang == Languages.English)
             typedId = int.Parse(inputField.text);
        else
            typedId = int.Parse(inputField_arab.text);

        continueButton.interactable = false;
        SessionManager.instance.IsEmployeeExists(typedId, EmployeeExistsCallBack);
    }

    private void EmployeeExistsCallBack(bool exists)
    {
        if (exists)
        {
            OnClickedContinueButton?.Invoke(this, new OnClickedContinueButtonEventArgs { employeeID = typedId });
        }
        else
        {
            StartCoroutine(TimedDisable());
        }
        continueButton.interactable = true;
    }

    private void OnClickedLanguage()
    {
        OnClickedLanguageButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        lang = language;
        
        if (language == Languages.English)
        {
            employeeIDInfoText_arab.gameObject.SetActive(false);
            inputField_arab.gameObject.SetActive(false);
            employeeIDInfoText.gameObject.SetActive(true);
            inputField.gameObject.SetActive(true);

            enterEmployeeIDText_arab.text = enterEmployeeID.english;

            continueButtonText_arab.text = continueContent.english;
            employeeIDInfoText.text = employeeIDInfo.english;
            inputFieldText.text = inputFieldContent.english;
            incorrectText_arab.text = incorrectContent.english;
        }
        else
        {
            employeeIDInfoText.gameObject.SetActive(false);
            inputField.gameObject.SetActive(false);
            employeeIDInfoText_arab.gameObject.SetActive(true);
            inputField_arab.gameObject.SetActive(true);

            enterEmployeeIDText_arab.text = enterEmployeeID.arabic;

            continueButtonText_arab.text = continueContent.arabic;
            // employeeIDInfoText.text = employeeIDInfo.arabic;
            employeeIDInfoText_arab.text = employeeIDInfo.arabic;
            // inputFieldText.text = inputFieldContent.arabic;
            inputFieldText_arab.text = inputFieldContent.arabic;
            incorrectText_arab.text = incorrectContent.arabic;
        }

    }

    private IEnumerator TimedDisable()
    {
        incorrectText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        incorrectText.gameObject.SetActive(false);
    }
}
