﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TitlePanel : UIPanel
{
    [SerializeField]
    private Button tapButton;
    public EventHandler OnTapped;

    private void Awake()
    {
        tapButton.onClick.AddListener(OnTap);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    private void OnTap()
    {
        OnTapped?.Invoke(this, new EventArgs());
    }

}
