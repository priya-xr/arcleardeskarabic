﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MainMenuPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text menuText;
    [SerializeField]
    private Fix3dTextCS menuText_arab;

    [SerializeField]
    private Button reScanButton;
    [SerializeField]
    private Button changeLanguageButton;
    [SerializeField]
    private Button exitToHomebutton;
    [SerializeField]
    private Button cancelButton;

    [SerializeField]
    private Text reScanButtonText;
    [SerializeField]
    private Fix3dTextCS reScanButtonText_arab;
    [SerializeField]
    private Text changeLanguageButtonText;
    [SerializeField]
    private Fix3dTextCS changeLanguageButtonText_arab;
    [SerializeField]
    private Text exitToHomebuttonText;
    [SerializeField]
    private Fix3dTextCS exitToHomebuttonText_arab;
    [SerializeField]
    private Text cancelButtonText;
    [SerializeField]
    private Fix3dTextCS cancelButtonText_arab;

    [SerializeField]
    private LanguagePanel languagePanel;


    [Header("Content")]
    public Content menu;
    public Content reScan;
    public Content changeLanguage;
    public Content exitToHome;
    public Content cancel;


    public EventHandler OnClickedReScanButton;
    public EventHandler OnClickedChangeLanguageButton;
    public EventHandler OnClickedExitToHomeButton;
    public EventHandler OnClickedCancelButton;

    public EventHandler OnClickedEnglishButton;
    public EventHandler OnClickedArabicButton;

    private Languages currentLanguage;


    private void Awake()
    {
        reScanButton.onClick.AddListener(OnClickedReScan);
        changeLanguageButton.onClick.AddListener(OnClickedChangeLanguage);
        exitToHomebutton.onClick.AddListener(OnClickedExitToHome);
        cancelButton.onClick.AddListener(OnClickedCancel);

        languagePanel.OnClickedArabicButton += OnClickedArabic;
        languagePanel.OnClickedEnglishButton += OnClickedEnglish;
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {

    }

    private void OnClickedReScan()
    {
        OnClickedReScanButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedChangeLanguage()
    {
        OnClickedChangeLanguageButton?.Invoke(this, new EventArgs());

        languagePanel.Show();
    }

    private void OnClickedExitToHome()
    {
        OnClickedExitToHomeButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedCancel()
    {
        OnClickedCancelButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedEnglish(System.Object sender, EventArgs args)
    {
        OnClickedEnglishButton?.Invoke(this, new EventArgs());
        languagePanel.Hide();
    }

    private void OnClickedArabic(System.Object sender, EventArgs args)
    {
        OnClickedArabicButton?.Invoke(this, new EventArgs());
        languagePanel.Hide();
    }

    public void UpdateContentLanguage(Languages language)
    {
        currentLanguage = language;

        if (currentLanguage == Languages.English)
        {
            menuText_arab.text = menu.english;
            reScanButtonText_arab.text = reScan.english;
            changeLanguageButtonText_arab.text = changeLanguage.english;
            exitToHomebuttonText_arab.text = exitToHome.english;
            cancelButtonText_arab.text = cancel.english;
        }
        else
        {
            menuText_arab.text = menu.arabic;
            reScanButtonText_arab.text = reScan.arabic;
            changeLanguageButtonText_arab.text = changeLanguage.arabic;
            exitToHomebuttonText_arab.text = exitToHome.arabic;
            cancelButtonText_arab.text = cancel.arabic;
        }
    }
}
