﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ModuleFinishPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text moduleIDText;
    [SerializeField]
    private Fix3dTextCS moduleIDText_arab;
    [SerializeField]
    private Text moduleFinishText;
    [SerializeField]
    private Fix3dTextCS moduleFinishText_arab;
    [SerializeField]
    private Text moduleNameText;
    [SerializeField]
    private Fix3dTextCS moduleNameText_arab;
    [SerializeField]
    private Button proceedButton;
    [SerializeField]
    private Button redoButton;
    [SerializeField]
    private Button submitButton;
    [SerializeField]
    private Button mainMenuButton;

    [SerializeField]
    private Text proceedButtonText;
    [SerializeField]
    private Fix3dTextCS proceedButtonText_arab;
    [SerializeField]
    private Text redoButtonText;
    [SerializeField]
    private Fix3dTextCS redoButtonText_arab;
    [SerializeField]
    private Text submitButtonText;
    [SerializeField]
    private Fix3dTextCS submitButtonText_arab;
    private Content[] contents;


    [Header("Content")]
    public Content successfullyFinish;
    public Content proceed;
    public Content redo;
    public Content submit;

    public EventHandler OnClickedProceedButton;
    public EventHandler OnClickedRedoButton;
    public EventHandler OnClickedSubmitButton;
    public EventHandler OnClickedMainMenuButton;

    private Languages currentLanguage;



    private void Awake()
    {
        proceedButton.onClick.AddListener(OnClickedProceed);
        redoButton.onClick.AddListener(OnClickedRedo);
        submitButton.onClick.AddListener(OnClickedSubmit);
        mainMenuButton.onClick.AddListener(OnClickedMainMenu);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        //0-module id text
        //1-finish text
        //2-proceed button state
        //3-submit button state
        //4-module name

        if (args.Length == 0)
            return;

        Content[] contents = new Content[5];
        contents[0] = args[0] as Content;
        contents[1] = args[1] as Content;
        contents[2] = args[2] as Content;
        contents[3] = args[3] as Content;
        contents[4] = args[4] as Content;


        string proceedState = args[2] as string;
        string submitState = args[3] as string;

        this.contents = contents;

        proceedButton.gameObject.SetActive(proceedState.Equals("true"));
        submitButton.gameObject.SetActive(submitState.Equals("true"));

        UpdateContentLanguage(currentLanguage);
    }

    private void OnClickedProceed()
    {
        OnClickedProceedButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedRedo()
    {
        OnClickedRedoButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedSubmit()
    {
        OnClickedSubmitButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        currentLanguage = language;

        if (contents != null)
        {
            if (contents.Length > 0)
            {
                if (currentLanguage == Languages.English)
                {
                    moduleIDText_arab.text = contents[0].english;
                    moduleNameText_arab.text = contents[4].english;
                }
                else
                {
                    moduleIDText_arab.text = contents[0].arabic;
                    moduleNameText_arab.text = contents[4].arabic;
                }
            }
        }


        if (currentLanguage == Languages.English)
        {
            //static 
            proceedButtonText_arab.text = proceed.english;
            redoButtonText_arab.text = redo.english;
            submitButtonText_arab.text = submit.english;
            moduleFinishText_arab.text = successfullyFinish.english;
        }
        else
        {
            //static 
            proceedButtonText_arab.text = proceed.arabic;
            redoButtonText_arab.text = redo.arabic;
            submitButtonText_arab.text = submit.arabic;
            moduleFinishText_arab.text = successfullyFinish.arabic;
        }
    }

    private void OnClickedMainMenu()
    {
        OnClickedMainMenuButton?.Invoke(this, new EventArgs());
    }
}
