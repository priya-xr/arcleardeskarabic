﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TrainingScorePanel : UIPanel, ILanguage
{

    [SerializeField]
    private Text trainingReportText;
    [SerializeField]
    private Fix3dTextCS trainingReportText_arab;
    [SerializeField]
    private Text employeeIDInfotext;
    [SerializeField]
    private Fix3dTextCS employeeIDInfotext_arab;
    [SerializeField]
    private Text employeeIDtext;
    [SerializeField]
    private Text totalTimeTakenText;
    [SerializeField]
    private Fix3dTextCS totalTimeTakenText_arab;
    [SerializeField]
    private Text finalTimeTakenText;
    [SerializeField]
    private Text moduleWiseTimeText;
    [SerializeField]
    private Fix3dTextCS moduleWiseTimeText_arab;
    [SerializeField]
    private Text module1Text;
    [SerializeField]
    private Fix3dTextCS module1Text_arab;
    [SerializeField]
    private Text module2Text;
    [SerializeField]
    private Fix3dTextCS module2Text_arab;
    [SerializeField]
    private Text module3Text;
    [SerializeField]
    private Fix3dTextCS module3Text_arab;

    [SerializeField]
    private Text module1TimeText;
    [SerializeField]
    private Text module2TimeText;
    [SerializeField]
    private Text module3TimeText;

    [SerializeField]
    private Button saveAndExitButton;
    [SerializeField]
    private Text saveAndExitButtonText;
    [SerializeField]
    private Fix3dTextCS saveAndExitButtonText_arab;

    [SerializeField]
    private Button mainMenuButton;

    [Header("Content")]
    public Content trainingReport;
    public Content employeeID;
    public Content finalTime;
    public Content modulewiseTime;
    public Content module1;
    public Content module2;
    public Content module3;
    public Content saveAndExit;

    private Languages currentLanguage;



    public EventHandler OnClickedSaveAndExitButton;
    public EventHandler OnClickedMainMenuButton;


    private void Awake()
    {
        saveAndExitButton.onClick.AddListener(OnClickedSaveAndExit);
        mainMenuButton.onClick.AddListener(OnClickedMainMenu);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        if (args.Length == 0)
            return;

        //0-employee id
        //1-total time
        //2-module 1 time
        //3-module 2 time
        //4-module 3 time

        employeeIDtext.text = args[0] as string;
        finalTimeTakenText.text = args[1] as string;

        module1TimeText.text = args[2] as string;
        module2TimeText.text = args[3] as string;
        module3TimeText.text = args[4] as string;

        UpdateContentLanguage(currentLanguage);
    }

    private void OnClickedSaveAndExit()
    {
        OnClickedSaveAndExitButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        currentLanguage = language;

        if (currentLanguage == Languages.English)
        {
            trainingReportText_arab.text = trainingReport.english;
            employeeIDInfotext_arab.text = employeeID.english;
            totalTimeTakenText_arab.text = finalTime.english;

            moduleWiseTimeText_arab.text = modulewiseTime.english;
            module1Text_arab.text = module1.english;
            module2Text_arab.text = module2.english;
            module3Text_arab.text = module3.english;

            saveAndExitButtonText_arab.text = saveAndExit.english;
        }
        else
        {
            trainingReportText_arab.text = trainingReport.arabic;
            employeeIDInfotext_arab.text = employeeID.arabic;
            totalTimeTakenText_arab.text = finalTime.arabic;

            moduleWiseTimeText_arab.text = modulewiseTime.arabic;
            module1Text_arab.text = module1.arabic;
            module2Text_arab.text = module2.arabic;
            module3Text_arab.text = module3.arabic;

            saveAndExitButtonText_arab.text = saveAndExit.arabic;
        }

    }

    private void OnClickedMainMenu()
    {
        OnClickedMainMenuButton?.Invoke(this, new EventArgs());
    }

}
