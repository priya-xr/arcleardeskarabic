﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CalibrationPanel : UIPanel
{
    [SerializeField]
    private Image moveImage;
    [SerializeField]
    private Text moveText;
    [SerializeField]
    private Fix3dTextCS moveText_arab;

    [SerializeField]
    private Button placeButton;
    [SerializeField]
    private Button languageButton;
    [SerializeField]
    private Button homeButton;
    [SerializeField]
    private Text placeButtonText;
    [SerializeField]
    private Fix3dTextCS placeButtonText_arab;

    public Content move;
    public Content place;
    [HideInInspector]
    public LanguageManager languageManager;

    public EventHandler OnClickedPlaceButton;
    public EventHandler OnClickedHomeButton;
    public EventHandler OnClickedLanguageButton;

    private Languages currentLanguage;




    private void Awake()
    {
        placeButton.onClick.AddListener(OnClickedPlace);
        homeButton.onClick.AddListener(OnClickedHome);
        languageButton.onClick.AddListener(OnClickedLanguage);
        languageManager = LanguageManager.instance;
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        //0-place button intractbale

        string active = args[0] as string;
        bool placeButtonActive=active.Equals("true") ? true : false;
        placeButton.gameObject.SetActive(placeButtonActive);
        moveImage.gameObject.SetActive(!placeButtonActive);
    }

    private void OnClickedPlace()
    {
        OnClickedPlaceButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedHome()
    {
        OnClickedHomeButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedLanguage()
    {
        OnClickedLanguageButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContent(Languages language)
    {
        currentLanguage = language;
       
        if (languageManager.CurrentLaguage == Languages.English)
        {
          //  placeButtonText_arab.text = place.english;
            moveText_arab.text = move.english;
        }
        else if(languageManager.CurrentLaguage == Languages.Arabic)
        {
          //  placeButtonText_arab.text = place.arabic;
            moveText_arab.text = move.arabic;
        }
    }
}
