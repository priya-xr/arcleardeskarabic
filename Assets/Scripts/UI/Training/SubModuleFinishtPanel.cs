﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SubModuleFinishtPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text moduleNameText;
    [SerializeField]
    private Fix3dTextCS moduleNameText_arab;
    [SerializeField]
    private Text subModuleNameText;
    [SerializeField]
    private Fix3dTextCS subModuleNameText_arab;
    [SerializeField]
    private Text subModuleDescriptionText;
    [SerializeField]
    private Fix3dTextCS subModuleDescriptionText_arab;
    [SerializeField]
    private Fix3dTextCS nextButtonText_arab;


    [SerializeField]
    private Button mainMenuButton;
    [SerializeField]
    private Button nextButton;

    private Content[] contents;


    [Header("Static Content")]
    public Content next;

    public EventHandler OnClickedMainMenuButton;
    public EventHandler OnClickedNextButton;


    private Languages currentLanguage;


    private void Awake()
    {
        mainMenuButton.onClick.AddListener(OnClickedMainMenu);
        nextButton.onClick.AddListener(OnClickedNext);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        if (args.Length == 0)
            return;

        //0-module name text
        //1-sub module name text
        //2-sub module description text

        Content[] contents = new Content[3];
        contents[0] = args[0] as Content;
        contents[1] = args[1] as Content;
        contents[2] = args[2] as Content;

        this.contents = contents;
        UpdateContentLanguage(currentLanguage);
    }

    private void OnClickedMainMenu()
    {
        OnClickedMainMenuButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedNext()
    {
        OnClickedNextButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        currentLanguage = language;

        if (contents != null)
        {
            if (contents.Length > 0)
            {
                if (currentLanguage == Languages.English)
                {
                    moduleNameText_arab.text = contents[0].english;
                    subModuleNameText_arab.text = contents[1].english;
                    subModuleDescriptionText_arab.text = contents[2].english;
                }
                else
                {
                    moduleNameText_arab.text = contents[0].arabic;
                    subModuleNameText_arab.text = contents[1].arabic;
                    subModuleDescriptionText_arab.text = contents[2].arabic;
                }
            }
        }

         if (currentLanguage == Languages.English)
        {
            //static 
            nextButtonText_arab.text = next.english;
        }
        else
        {
            //static 
            nextButtonText_arab.text = next.arabic;
        }
    }
}
