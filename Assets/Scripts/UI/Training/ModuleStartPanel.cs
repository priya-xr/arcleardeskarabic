﻿using System;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class ModuleStartPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text moduleIDText;
    [SerializeField]
    private Fix3dTextCS moduleIDText_arab;
    [SerializeField]
    private Text moduleNameText;
    [SerializeField]
    private Fix3dTextCS moduleNameText_arab;
    [SerializeField]
    private Text moduleDescriptionText;
    [SerializeField]
    private Fix3dTextCS moduleDescriptionText_arab;

    [SerializeField]
    private Button startButton;
    [SerializeField]
    private Text startButtonText;
    [SerializeField]
    private Fix3dTextCS startButtonText_arab;
    [SerializeField]
    private Button skipButton;
    [SerializeField]
    private Text skipButtonText;
    [SerializeField]
    private Fix3dTextCS skipButtonText_arab;
    [SerializeField]
    private Button mainMenuButton;

    private Content[] contents;


    [Header("Static Content")]
    public Content start;
    public Content skip;

    public EventHandler OnClickedStartButton;
    public EventHandler OnClickedSkipButton;
    public EventHandler OnClickedMainMenuButton;


    private Languages currentLanguage;


    private void Awake()
    {
        startButton.onClick.AddListener(OnClickedStart);
        skipButton.onClick.AddListener(OnClickedSkip);
        mainMenuButton.onClick.AddListener(OnClickedMainMenu);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        if (args.Length == 0)
            return;

        //0-module id text
        //1-module name text
        //2-model description text
        //3-skip button intractable

        Content[] contents = new Content[3];
        contents[0] = args[0] as Content;
        contents[1] = args[1] as Content;
        contents[2] = args[2] as Content;

        this.contents = contents;

        string intractable=args[3] as string;
        
        skipButton.interactable=intractable.Equals("true");
        UpdateContentLanguage(currentLanguage);
    }

    private void OnClickedStart()
    {
        OnClickedStartButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedSkip()
    {
        OnClickedSkipButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedMainMenu()
    {
        OnClickedMainMenuButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        currentLanguage = language;

        if (contents != null)
        {
            if (contents.Length > 0)
            {
                if (currentLanguage == Languages.English)
                {
                    moduleIDText_arab.text = contents[0].english;
                    moduleNameText_arab.text = contents[1].english;
                    moduleDescriptionText_arab.text = contents[2].english;
                }
                else
                {
                    moduleIDText_arab.text = contents[0].arabic;
                    moduleNameText_arab.text = contents[1].arabic;
                    moduleDescriptionText_arab.text = contents[2].arabic;
                }
            }
        }

        if (currentLanguage == Languages.English)
        {
            //static 
            startButtonText_arab.text = start.english;
            skipButtonText_arab.text = skip.english;
        }
        else
        {
            //static 
            startButtonText_arab.text = start.arabic;
            skipButtonText_arab.text = skip.arabic;
        }

    }
}
