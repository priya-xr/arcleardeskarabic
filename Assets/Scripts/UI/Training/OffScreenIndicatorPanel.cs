﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class OffScreenIndicatorPanel : UIPanel, ILanguage
{
    [SerializeField]
    private TrainingGameUI trainingGameUI;
    private OffScreenIndicator offScreenIndicator;

    [SerializeField]
    private RectTransform iconParent;
    [SerializeField]
    private Image icon;
    [SerializeField]
    private Image rotationIcon;
    [SerializeField]
    private Text iconNameText;

    private Content itemName;

    private Languages currentLanguage;


    private void Awake()
    {
        offScreenIndicator = trainingGameUI.TrainingGame.OffScreenIndicator;
        offScreenIndicator.OnIndicatorModified += OnOffScreenIndicatorModified;
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        //0 -item name
        if (args.Length == 0)
            return;

        itemName = args[0] as Content;

        UpdateContentLanguage(currentLanguage);
    }

    private void OnOffScreenIndicatorModified(object sender, OffScreenIndicator.OnOffScreenIndicatorModifiedArgs args)
    {
        iconParent.gameObject.SetActive(args.isOffScreen);
        iconParent.transform.position = args.indicatorWorldPosition;

        iconParent.localEulerAngles = args.rotation;
    }
    public void UpdateContentLanguage(Languages language)
    {
        currentLanguage = language;

        if (itemName == null)
            return;

        if (currentLanguage == Languages.English)
        {
            //iconNameText.text = itemName.english;
        }
        else
        {
            //iconNameText.text = itemName.arabic;
        }

    }
}
