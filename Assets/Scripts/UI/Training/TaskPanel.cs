﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TaskPanel : UIPanel, ILanguage
{
    [SerializeField]
    private UIPanel[] subPanels;
    [SerializeField]
    private Button mainMenuButton;
    [SerializeField]
    private MiniGameUI miniGameUI;
    private Game game;
    private Inventory inventory;
    //[SerializeField]
    //private Button FixButton;

    public EventHandler OnClickedMainMenuButton;
    public EventHandler OnClickedFixButton;

    private void Awake()
    {
        mainMenuButton.onClick.AddListener(OnClickedMainMenu);
       // FixButton.onClick.AddListener(OnClickedFix);
    }

    private void Start()
    {
        game = miniGameUI.MiniGame as Game;
        game.Inventory.OnItemAdded += OnItemAdded;
        game.Inventory.OnItemRemoved += OnItemRemoved;
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {

    }

    public void UpdateUISubPanel(string subPanelName, object[] args)
    {
        GetUiSubPanel(subPanelName).UpdateUIPanel(args);
    }

    public void ShowSubPanel(string subPanelName)
    {
        UIPanel subPanel = GetUiSubPanel(subPanelName);
        if (subPanel != null)
        {
            subPanel.Show();
        }
    }

    public void HideSubPanel(string subPanelName)
    {
        UIPanel subPanel = GetUiSubPanel(subPanelName);
        if (subPanel != null)
        {
            subPanel.Hide();
        }
    }

    private UIPanel GetUiSubPanel(string subPanelName)
    {
        for (int i = 0; i < subPanels.Length; i++)
        {
            if (subPanelName.ToLower() == subPanels[i].Name.ToLower())
            {
                return subPanels[i];
            }
        }
        return null;
    }

    private void OnItemAdded(object sender, Inventory.OnItemAddedEventArgs args)
    {
        Item item = args.addedItem;

        Content itemName = item.ItemID;
        Sprite itemImage = item.icon;
        UpdateUISubPanel("InventoryPanel", new object[2] { itemName, itemImage });
        ShowSubPanel("InventoryPanel");
    }

    private void OnItemRemoved(object sender, Inventory.OnItemRemovedEventArgs args)
    {
        HideSubPanel("InventoryPanel");
    }

    public void UpdateContentLanguage(Languages language)
    {
        for (int i = 0; i < subPanels.Length; i++)
        {
            if (subPanels[i] == null)
                return;

            ILanguage ILanguage = subPanels[i].GetComponent<ILanguage>();

            if (ILanguage != null)
            {
                ILanguage.UpdateContentLanguage(language);
            }
        }
    }

    private void OnClickedMainMenu()
    {
        OnClickedMainMenuButton?.Invoke(this, new EventArgs());
    }

    //private void OnClickedFix()
    //{
    //    OnClickedFixButton?.Invoke(this, new EventArgs());
    //}
}

