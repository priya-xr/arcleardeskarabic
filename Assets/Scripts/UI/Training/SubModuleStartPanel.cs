﻿    using System;
using UnityEngine;
using UnityEngine.UI;

public class SubModuleStartPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text moduleNameText;
    [SerializeField]
    private Fix3dTextCS moduleNameText_arab;
    [SerializeField]
    private Text subModuleNameText;
    [SerializeField]
    private Fix3dTextCS subModuleNameText_arab;
    [SerializeField]
    private Text subModuleDescriptionText;
    [SerializeField]
    private Fix3dTextCS subModuleDescriptionText_arab;

    [SerializeField]
    private Button startButton;
    [SerializeField]
    private Text startButtonText;
    [SerializeField]
    private Fix3dTextCS startButtonText_arab;
    [SerializeField]
    private Button mainMenuButton;

    private Content[] contents;


    [Header("Static Content")]
    public Content start;

    public EventHandler OnClickedStartButton;
    public EventHandler OnClickedMainMenuButton;


    private Languages currentLanguage;


    private void Awake()
    {
        startButton.onClick.AddListener(OnClickedStart);
        mainMenuButton.onClick.AddListener(OnClickedMainMenu);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        if (args.Length == 0)
            return;

        //0-module naame
        //1-sub module naame
        //2-description

        Content[] contents = new Content[3];
        contents[0] = args[0] as Content;
        contents[1] = args[1] as Content;
        contents[2] = args[2] as Content;

        this.contents = contents;        
        UpdateContentLanguage(currentLanguage);
    }

    private void OnClickedStart()
    {
        OnClickedStartButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedMainMenu()
    {
        OnClickedMainMenuButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        currentLanguage = language;

        if (contents != null)
        {
            if (contents.Length > 0)
            {
                if (currentLanguage == Languages.English)
                {
                    moduleNameText_arab.text = contents[0].english;
                    subModuleNameText_arab.text = contents[1].english;
                    subModuleDescriptionText_arab.text = contents[2].english;
                }
                else
                {
                    moduleNameText_arab.text = contents[0].arabic;
                    subModuleNameText_arab.text = contents[1].arabic;
                    subModuleDescriptionText_arab.text = contents[2].arabic;
                }
            }
        }

        if (currentLanguage == Languages.English)
        {
            //static 
            startButtonText_arab.text = start.english;
        }
        else
        {
            //static 
            startButtonText_arab.text = start.arabic;
        }

    }
}
