﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TaskInfoPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text taskIDText;
    [SerializeField]
    private Fix3dTextCS taskIDText_arab;
    [SerializeField]
    private Text moduleNameText;
    [SerializeField]
    private Fix3dTextCS moduleNameText_arab;
    [SerializeField]
    private Text taskNameText;
    [SerializeField]
    private Fix3dTextCS taskNameText_arab;
    [SerializeField]
    private Text taskDescriptionText;
    [SerializeField]
    private Fix3dTextCS taskDescriptionText_arab;
    [SerializeField]
    private GameObject bgRoot;
    private Content[] contents;


    public EventHandler OnClickedTaskMenuButton;

    private Languages currentLanguage;

    private void Awake()
    {
        
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        //0-task id text
        //1-module name text
        //2-Task header text
        //3-Task description text

        if (args.Length == 0)
            return;

        Content[] contents = new Content[4];
        contents[0] = args[0] as Content;
        contents[1] = args[1] as Content;
        contents[2] = args[2] as Content;
        contents[3] = args[3] as Content;

        this.contents = contents;
        UpdateContentLanguage(currentLanguage);
    }
    private void OnClickedTaskMenu()
    {
        bgRoot.SetActive(!bgRoot.activeSelf);

        OnClickedTaskMenuButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        currentLanguage = language;

        if (contents != null)
        {
            if (contents.Length > 0)
            {
                if (currentLanguage == Languages.English)
                {
                    taskIDText_arab.text = contents[0].english;
                    moduleNameText_arab.text = contents[1].english;
                    taskNameText_arab.text = contents[2].english;
                    taskDescriptionText_arab.text = contents[3].english;
                }
                else
                {
                    taskIDText_arab.text = contents[0].arabic;
                    moduleNameText_arab.text = contents[1].arabic;
                    taskNameText_arab.text = contents[2].arabic;
                    taskDescriptionText_arab.text = contents[3].arabic;
                }
            }

        }
    }
}
