﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPanel : UIPanel
{
    private void Awake()
    {
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

}