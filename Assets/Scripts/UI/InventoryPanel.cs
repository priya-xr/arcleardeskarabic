﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text itemInHandText;
    [SerializeField]
    private Fix3dTextCS itemInHandText_arab;
    [SerializeField]
    private Text itemNameText;
    [SerializeField]
    private Fix3dTextCS itemNameText_arab;
    [SerializeField]
    private Image itemImage;
    private Content itemNameContent;


    [Header("Content")]
    public Content itemInHand;


    public EventHandler OnClickedInventoryMenuButton;

    private Languages currentLanguage;

    private void Awake()
    {
        Hide();
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        //0-item name
        //1-item sprite

        if (args.Length == 0)
            return;

        itemNameContent = args[0] as Content;
        itemImage.sprite = args[1] as Sprite;

        UpdateContentLanguage(currentLanguage);
    }
    private void OnClickedInventoryMenu()
    {
        OnClickedInventoryMenuButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        currentLanguage = language;

        if (itemNameContent != null)
        {
            if (currentLanguage == Languages.English)
            {
                itemNameText_arab.text = itemNameContent.english;
            }
            else
            {
                itemNameText_arab.text = itemNameContent.arabic;
            }
        }

        if (currentLanguage == Languages.English)
        {
            itemInHandText_arab.text = itemInHand.english;
        }
        else
        {
            itemInHandText_arab.text = itemInHand.arabic;
        }
    }
}
