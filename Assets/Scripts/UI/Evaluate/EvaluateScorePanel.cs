﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EvaluateScorePanel : UIPanel, ILanguage
{

    [SerializeField]
    private Text evaluationReportText;
    [SerializeField]
    private Fix3dTextCS evaluationReportText_arab;
    [SerializeField]
    private Text employeeIDInfotext;
    [SerializeField]
    private Fix3dTextCS employeeIDInfotext_arab;
    [SerializeField]
    private Text employeeIDtext;
    [SerializeField]
    private Text finalScoreInfoText;
    [SerializeField]
    private Fix3dTextCS finalScoreInfoText_arab;
    [SerializeField]
    private Text finalScoreText;
    [SerializeField]
    private Text moduleWiseScoreText;
    [SerializeField]
    private Fix3dTextCS moduleWiseScoreText_arab;
    [SerializeField]
    private Text module1Text;
    [SerializeField]
    private Fix3dTextCS module1Text_arab;
    [SerializeField]
    private Text module2Text;
    [SerializeField]
    private Fix3dTextCS module2Text_arab;
    [SerializeField]
    private Text module3Text;
    [SerializeField]
    private Fix3dTextCS module3Text_arab;

    [SerializeField]
    private Text module1ScoreText;
    [SerializeField]
    private Text module2ScoreText;
    [SerializeField]
    private Text module3ScoreText;

    [SerializeField]
    private Button saveAndExitButton;
    [SerializeField]
    private Text saveAndExitButtonText;
    [SerializeField]
    private Fix3dTextCS saveAndExitButtonText_arab;

    [SerializeField]
    private Button mainMenuButton;

    [Header("Content")]
    public Content evaluationReport;
    public Content employeeID;
    public Content finalScore;
    public Content modulewiseScore;
    public Content module1;
    public Content module2;
    public Content module3;
    public Content saveAndExit;


    public EventHandler OnClickedMainMenuButton;
    public EventHandler OnClickedSaveAndExitButton;


    private void Awake()
    {
        saveAndExitButton.onClick.AddListener(OnClickedSaveAndExit);
        mainMenuButton.onClick.AddListener(OnClickedMainMenu);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        if (args.Length == 0)
            return;

        //0-employee id
        //1-score
        //2-module 1 score
        //3-module 2 score
        //4-module 3 score

        employeeIDtext.text = args[0] as string;
        finalScoreText.text = args[1] as string;

        module1ScoreText.text = args[2] as string;
        module2ScoreText.text = args[3] as string;
        module3ScoreText.text = args[4] as string;
    }

    private void OnClickedSaveAndExit()
    {
        OnClickedSaveAndExitButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedMainMenu()
    {
        OnClickedMainMenuButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        if (language == Languages.English)
        {
            evaluationReportText_arab.text = evaluationReport.english;
            employeeIDInfotext_arab.text = employeeID.english;
            finalScoreInfoText_arab.text = finalScore.english;

            moduleWiseScoreText_arab.text = modulewiseScore.english;
            module1Text_arab.text = module1.english;
            module2Text_arab.text = module2.english;
            module3Text_arab.text = module3.english;

            saveAndExitButtonText_arab.text = saveAndExit.english;
        }
        else
        {
            evaluationReportText_arab.text = evaluationReport.arabic;
            employeeIDInfotext_arab.text = employeeID.arabic;
            finalScoreInfoText_arab.text = finalScore.arabic;

            moduleWiseScoreText_arab.text = modulewiseScore.arabic;
            module1Text_arab.text = module1.arabic;
            module2Text_arab.text = module2.arabic;
            module3Text_arab.text = module3.arabic;

            saveAndExitButtonText_arab.text = saveAndExit.arabic;
        }

    }
}
