﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EvaluateinfoPanel : UIPanel,ILanguage
{
    [SerializeField]
    private Text evalautionText;
    [SerializeField]
    private Fix3dTextCS evalautionText_arab;
    [SerializeField]
    private Text clearDeskText;
    [SerializeField]
    private Fix3dTextCS clearDeskText_arab;
    [SerializeField]
    private Text timeleftText;
    [SerializeField]
    private Fix3dTextCS timeleftText_arab;
    [SerializeField]
    private Text timeText;

    [SerializeField]
    private Button submitButton;
    [SerializeField]
    private Text submitButtonText;
    [SerializeField]
    private Fix3dTextCS submitButtonText_arab;

    [Header("Content")]
    public Content evaluation;
    public Content clearDesk;
    public Content timeLeft;
    public Content submitTest;


    public EventHandler OnClickedSubmitButton;

    private void Awake()
    {
        submitButton.onClick.AddListener(OnClickedSubmit);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        if (args.Length == 0)
            return;
        //0-time left
        string timeLeft = args[0] as string;
        timeText.text = timeLeft;
    }

    private void OnClickedSubmit()
    {
        OnClickedSubmitButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        if (language == Languages.English)
        {
            evalautionText_arab.text = evaluation.english;
            clearDeskText_arab.text = clearDesk.english;
            timeleftText_arab.text = timeLeft.english;

            submitButtonText_arab.text = submitTest.english;
        }
        else
        {
            evalautionText_arab.text = evaluation.arabic;
            clearDeskText_arab.text = clearDesk.arabic;
            timeleftText_arab.text = timeLeft.arabic;

            submitButtonText_arab.text = submitTest.arabic;
        }

    }
}
