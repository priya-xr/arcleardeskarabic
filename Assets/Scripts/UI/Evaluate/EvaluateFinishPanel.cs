﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EvaluateFinishPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text evaluationText;
    [SerializeField]
    private Fix3dTextCS evaluationText_arab;
    [SerializeField]
    private Text confirmText;
    [SerializeField]
    private Fix3dTextCS confirmText_arab;
    [SerializeField]
    private Text descriptionText;
    [SerializeField]
    private Fix3dTextCS descriptionText_arab;

    [SerializeField]
    private Button submitButton;
    [SerializeField]
    private Button cancelButton;
    [SerializeField]
    private Text submitButtonText;
    [SerializeField]
    private Fix3dTextCS submitButtonText_arab;
    [SerializeField]
    private Text cancelButtonText;
    [SerializeField]
    private Fix3dTextCS cancelButtonText_arab;

    [SerializeField]
    private Button mainMenuButton;


    [Header("Content")]
    public Content evaluation;
    public Content confirmSubmit;
    public Content description;
    public Content submit;
    public Content cancel;


    public EventHandler OnClickedSubmitButton;
    public EventHandler OnClickedCancelButton;
    public EventHandler OnClickedMainMenuButton;


    private void Awake()
    {
        submitButton.onClick.AddListener(OnClickedSubmit);
        cancelButton.onClick.AddListener(OnClickedCancel);
        mainMenuButton.onClick.AddListener(OnClickedMainMenu);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {

    }

    private void OnClickedSubmit()
    {
        OnClickedSubmitButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedMainMenu()
    {
        OnClickedMainMenuButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedCancel()
    {
        OnClickedCancelButton?.Invoke(this, new EventArgs());
    }

    public void UpdateContentLanguage(Languages language)
    {
        if (language == Languages.English)
        {
            evaluationText_arab.text = evaluation.english;
            confirmText_arab.text = confirmSubmit.english;
            descriptionText_arab.text = description.english;

            submitButtonText_arab.text = submit.english;
            cancelButtonText_arab.text = cancel.english;
        }
        else
        {
            evaluationText_arab.text = evaluation.arabic;
            confirmText_arab.text = confirmSubmit.arabic;
            descriptionText_arab.text = description.arabic;

            submitButtonText_arab.text = submit.arabic;
            cancelButtonText_arab.text = cancel.arabic;
        }

    }
}
