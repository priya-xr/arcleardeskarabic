﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EvaluateStartPanel : UIPanel, ILanguage
{
    [SerializeField]
    private Text evaluationText;
    [SerializeField]
    private Fix3dTextCS evaluationText_arab;
    [SerializeField]
    private Text taskCountText;
    [SerializeField]
    private Fix3dTextCS taskCountText_arab;
    [SerializeField]
    private Text taskTimeText;
    [SerializeField]
    private Fix3dTextCS taskTimeText_arab;
    [SerializeField]
    private Text clearDeskText;
    [SerializeField]
    private Fix3dTextCS clearDeskText_arab;
    [SerializeField]
    private Text descriptionText;
    [SerializeField]
    private Fix3dTextCS descriptionText_arab;


    [SerializeField]
    private Button startButton;
    [SerializeField]
    private Text startButtonText;
    [SerializeField]
    private Fix3dTextCS startButtonText_arab;
    [SerializeField]
    private Button exitButton;
    [SerializeField]
    private Text exitButtonText;
    [SerializeField]
    private Fix3dTextCS exitButtonText_arab;

    [SerializeField]
    private Button mainMenuButton;


    [Header("Content")]
    public Content evaluation;
    public Content tasks;
    public Content time;
    public Content clearDesk;
    public Content description;
    public Content start;
    public Content exit;



    public EventHandler OnClickedStartButton;
    public EventHandler OnClickedExitButton;
    public EventHandler OnClickedMainMenuButton;


    private void Awake()
    {
        startButton.onClick.AddListener(OnClickedStart);
        exitButton.onClick.AddListener(OnClickedExit);
        mainMenuButton.onClick.AddListener(OnClickedMainMenu);
    }

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void UpdateUIPanel(object[] args)
    {
        if (args.Length == 0)
            return;

        //0 task count
        string taskCount = args[0] as string;
        taskCountText.text = taskCount.ToString();
    }

    private void OnClickedStart()
    {
        OnClickedStartButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedExit()
    {
        OnClickedExitButton?.Invoke(this, new EventArgs());
    }

    private void OnClickedMainMenu()
    {
        OnClickedMainMenuButton?.Invoke(this, new EventArgs());
    }


    public void UpdateContentLanguage(Languages language)
    {
        if (language == Languages.English)
        {
            evaluationText_arab.text = evaluation.english;
            taskCountText_arab.text = tasks.english;
            taskTimeText_arab.text = time.english;

            clearDeskText_arab.text = clearDesk.english;
            descriptionText_arab.text = description.english;

            startButtonText_arab.text = start.english;
            exitButtonText_arab.text = exit.english;
        }
        else
        {
            evaluationText_arab.text = evaluation.arabic;
            taskCountText_arab.text = tasks.arabic;
            taskTimeText_arab.text = time.arabic;

            clearDeskText_arab.text = clearDesk.arabic;
            descriptionText_arab.text = description.arabic;

            startButtonText_arab.text = start.arabic;
            exitButtonText_arab.text = exit.arabic;
        }

    }
}
