﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BaseGameUI : MiniGameUI
{
    private BaseGame baseGame;

    [SerializeField]
    private LanguagePanel languagePanel;
    [SerializeField]
    private EmplloyeeIDPanel emplloyeeIDPanel;
    [SerializeField]
    private ModuleSelectPanel moduleSelectPanel;
    [SerializeField]
    private TitlePanel titlePanel;
    [SerializeField]
    private ResumePanel resumePanel;
    [SerializeField]
    private LessonPanel lessonPanel;

    private LanguageManager languageManager;


    private void Awake()
    {
        baseGame = miniGame as BaseGame;

        languagePanel.OnClickedArabicButton += OnClickedArabicButton;
        languagePanel.OnClickedEnglishButton += OnClickedEnglishButton;

        emplloyeeIDPanel.OnClickedLanguageButton += OnClickedLanguageSwapButton;
        emplloyeeIDPanel.OnClickedContinueButton += OnClickedContinueButton;

        moduleSelectPanel.OnClickedEvaluationButton += OnClickedEvaluationModuleButton;
        moduleSelectPanel.OnClickedTrainingButton += OnClickedTrainingModuleButton;
        moduleSelectPanel.OnClickedLangButton += OnClickedLanguageSwapButton;
        moduleSelectPanel.OnClickedHomeButton += OnClickedHomeButton;

        resumePanel.OnClickedHomeButton += OnClickedHomeButton;
        resumePanel.OnClickedLanguageSwapButton += OnClickedLanguageSwapButton;
        resumePanel.OnClickedResumeButton += OnClickedResume;
        resumePanel.OnClickedNewSessionButton += OnClickedNewSession;

        titlePanel.OnTapped += OnTappedTitlePanel;

        lessonPanel.OnClickedClearDeskButton += OnClickedClearDeskButton;
        lessonPanel.OnClickedHomeButton += OnClickedHomeButton;
        lessonPanel.OnClickedLangButton += OnClickedLanguageSwapButton;

    }

    private void Start()
    {
        languageManager = baseGame.languageManager;
        languageManager.OnLanguageChanged += OnLanguageChanged;
        baseGame.sessionManager.OnConnectionStatusChanged += OnConnectionStatusChanged;

        OnLanguageChanged(this, new LanguageManager.OnLanguageChangedArgs { currentLaguage = languageManager.CurrentLaguage });
    }

    public override void ShowPanel(string panelName, bool hideExisting)
    {
        if (hideExisting)
        {
            for (int i = 0; i < uiPanels.Length; i++)
            {
                uiPanels[i].Hide();
            }
        }

        UIPanel uiPanel = GetUiPanel(panelName);
        if (uiPanel != null)
        {
            uiPanel.Show();
        }
    }

    public override void HidePanel(string panelName)
    {
        UIPanel uiPanel = GetUiPanel(panelName);
        if (uiPanel != null)
        {
            uiPanel.Hide();
        }
    }


    protected override UIPanel GetUiPanel(string panelName)
    {
        for (int i = 0; i < uiPanels.Length; i++)
        {
            if (panelName.ToLower() == uiPanels[i].Name.ToLower())
            {
                return uiPanels[i];
            }
        }
        return null;
    }

    public override void UpdatePanelDetails(string panelName, object[] args)
    {
        UIPanel uiPanel = GetUiPanel(panelName);
        if (uiPanel != null)
        {
            uiPanel.UpdateUIPanel(args);
        }
    }

    private void OnClickedEnglishButton(object sender, EventArgs args)
    {
        baseGame.SetLanguage(Languages.English);
        ShowPanel("EmployeeIDPanel", true);
    }

    private void OnClickedArabicButton(object sender, EventArgs args)
    {
        baseGame.SetLanguage(Languages.Arabic);
        ShowPanel("EmployeeIDPanel", true);
    }

    private void OnClickedLanguageSwapButton(object sender, EventArgs args)
    {
        baseGame.ToggleLanguage();
    }


    private void OnClickedHomeButton(object sender, EventArgs args)
    {
        //reload scene
        GlobalValues.instance.BaseStates = BASESTATES.BEGIN;
        baseGame.LoadLevel("Start");
    }

    private void OnClickedClearDeskButton(object sender, EventArgs args)
    {
        ShowPanel("ModuleSelectPanel", true);
        // baseGame.ToggleLanguage();
    }

    private void OnClickedTrainingModuleButton(object sender, EventArgs args)
    {
        if ((baseGame.sessionManager.GetLastTrainingModuleIndex() == -1) ||
            (baseGame.sessionManager.GetLastTrainingModuleIndex() == 2))
        {
            GlobalValues.instance.TrainingState = TRAININGSTATES.NEWSESSION;
            baseGame.LoadLevel("Training");
        }
        else
        {
            string enable = baseGame.sessionManager.IsTrainingComplete().ToString();
            UpdatePanelDetails("ResumePanel", new object[] { enable });
            ShowPanel("ResumePanel", true);
        }

    }

    private void OnClickedEvaluationModuleButton(object sender, EventArgs args)
    {
        //load evaluation scene
        baseGame.LoadLevel("Evaluation");
    }

    private void OnTappedTitlePanel(object sender, EventArgs args)
    {
        ShowPanel("LanguageSelectPanel", true);
    }

    private void OnClickedContinueButton(object sender, EmplloyeeIDPanel.OnClickedContinueButtonEventArgs args)
    {
        int id = args.employeeID;
        baseGame.CreateSession(id,"AR",
        () =>
        {
            string evaluationState = baseGame.sessionManager.IsTrainingComplete().ToString().ToLower();
            UpdatePanelDetails("ModuleSelectPanel", new object[] { evaluationState });
            ShowPanel("LessonPanel", true);
            //ShowPanel("ModuleSelectPanel", true);
        });
    }

    private void OnClickedResume(object sender, EventArgs args)
    {
        GlobalValues.instance.TrainingState = TRAININGSTATES.RESUME;
        baseGame.LoadLevel("Training");
    }

    private void OnClickedNewSession(object sender, EventArgs args)
    {
        GlobalValues.instance.TrainingState = TRAININGSTATES.NEWSESSION;
        baseGame.LoadLevel("Training");
    }

    private void OnLanguageChanged(object sender, LanguageManager.OnLanguageChangedArgs args)
    {
        for (int i = 0; i < uiPanels.Length; i++)
        {
            if (uiPanels[i] == null)
                return;

            ILanguage ILanguage = uiPanels[i].GetComponent<ILanguage>();

            if (ILanguage != null)
            {
                ILanguage.UpdateContentLanguage(args.currentLaguage);
            }
        }
    }

    private void OnConnectionStatusChanged(object sender, SessionManager.OnConnectionStatusChangedArgs args)
    {
        if (!args.connected)
        {
            ShowPanel("ConnnectionFailedPanel", false);
        }
    }


}
