﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.XR.ARFoundation;


public class ARCalibration : MonoBehaviour
{
    public GameObject anchor;
    public GameObject aanchorVisual;
    public bool canScan = false;
    public float currentScanTime = 0f;
    public float scanMaxTime = 60f;

    public GameObject scanSuccessPanel;
    public GameObject scanningPanel;
    public GameObject scanningFailedPanel;

    public GameObject rootGameObejct;

    public ARRaycastManager arRaycastManager;

    void Start()
    {
        StartCalibration();

    }
    void Update()
    {

        if (canScan)
        {
            Vector3 screenCenterPoint = Camera.main.ScreenToViewportPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0f));
        }
    }

    public void StartCalibration()
    {
        rootGameObejct.SetActive(false);
        canScan = true;
    }

    public void StopCalibration()
    {
        canScan = false;
        scanSuccessPanel.SetActive(false);
        scanningPanel.SetActive(false);

        EnablePlaneDetection(false);
    }

    public void Place()
    {
        StopCalibration();

        aanchorVisual.SetActive(false);
        rootGameObejct.SetActive(true);
    }
    public void EnablePlaneDetection(bool enable)
    {
        if (enable)
        {
            /*ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration();
            config.planeDetection = UnityARPlaneDetection.Horizontal;
            config.alignment = UnityARAlignment.UnityARAlignmentGravity;
            config.getPointCloudData = true;
            config.enableLightEstimation = true;

            arCameraManager.m_session.RunWithConfig(config);*/

        }
        else
        {
            /*ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration();
            config.planeDetection = UnityARPlaneDetection.None;
            config.alignment = UnityARAlignment.UnityARAlignmentGravity;
            config.getPointCloudData = false;
            config.enableLightEstimation = false;

            arCameraManager.m_session.RunWithConfig(config);*/
        }

    }


    void ModifyVisuals(bool isPlaneDetected)
    {
        if (isPlaneDetected)
        {
            Debug.Log("Scan success");
            if (!scanSuccessPanel.activeSelf)
            {
                scanSuccessPanel.SetActive(true);
            }
            if (scanningPanel.activeSelf)
            {
                scanningPanel.SetActive(false);
            }
            if (scanningFailedPanel.activeSelf)
            {
                scanningFailedPanel.SetActive(false);
            }
            if (!aanchorVisual.activeSelf)
            {
                aanchorVisual.SetActive(true);
            }

            currentScanTime = 0f;
        }
        else if ((currentScanTime < scanMaxTime) && (!isPlaneDetected))
        {
            Debug.Log("Scanning");

            if (scanSuccessPanel.activeSelf)
            {
                scanSuccessPanel.SetActive(false);
            }
            if (!scanningPanel.activeSelf)
            {
                scanningPanel.SetActive(true);
            }
            if (scanningFailedPanel.activeSelf)
            {
                scanningFailedPanel.SetActive(false);
            }
            if (aanchorVisual.activeSelf)
            {
                aanchorVisual.SetActive(false);
            }
            currentScanTime += Time.deltaTime;
        }
        else if ((currentScanTime >= scanMaxTime) && (!isPlaneDetected))
        {
            Debug.Log("Scan failed");

            if (scanSuccessPanel.activeSelf)
            {
                scanSuccessPanel.SetActive(false);
            }
            if (scanningPanel.activeSelf)
            {
                scanningPanel.SetActive(false);
            }
            if (!scanningFailedPanel.activeSelf)
            {
                scanningFailedPanel.SetActive(true);
            }
            if (aanchorVisual.activeSelf)
            {
                aanchorVisual.SetActive(false);
            }
            currentScanTime = 0f;
            StopCalibration();
        }
    }

    public void ReCaliberate()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
