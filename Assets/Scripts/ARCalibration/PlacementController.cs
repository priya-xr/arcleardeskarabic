﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;


[RequireComponent(typeof(ARRaycastManager))]
public class PlacementController : MonoBehaviour
{

    [SerializeField]
    private GameObject placedPrefab;

    public Transform parent;
    public GameObject dummy;
    public GameObject actual;
    public GameObject canvas;


    public GameObject PlacedPrefab
    {
        get
        {
            return placedPrefab;
        }
        set
        {
            placedPrefab = value;
        }
    }

    private ARRaycastManager arRaycastManager;

    void Awake()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;

        return false;
    }


    void Update()
    {
        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;

        if (arRaycastManager.Raycast(touchPosition, hits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinBounds))
        {
            parent.gameObject.SetActive(true);

            var hitPose = hits[0].pose;
            parent.transform.position = hitPose.position;
            parent.transform.rotation = hitPose.rotation;
        }
        else
        {
            parent.gameObject.SetActive(false);
        }
    }

    public void OnClickedPlace()
    {
        if (!parent.gameObject.activeSelf)
            return;


        actual.SetActive(true);
        dummy.SetActive(false);
        canvas.SetActive(false);
        this.enabled = false;
    }

    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
}
