﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TapItem : Item, IIntractable, IHighlightable, IAnimatable, ISound
{
    [SerializeField]
    private float intractionTime;
    [SerializeField]
    private bool isIntratable;
    [SerializeField]
    private Animation animationComp;
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private ProgressBar progressBar;
    [SerializeField]
    private ItemOutline itemOutline;

    public EventHandler OnTapped;

    public float IntractionTime { get => intractionTime; set => intractionTime = value; }
    public bool IsIntratable { get => isIntratable; set => isIntratable = value; }

    private void Awake()
    {

    }
    public void Intract()
    {
        if (!isIntratable)
            return;

        progressBar.StartProgress(IntractionTime);
        progressBar.OnComplete += OnProgressComplete;
    }

    public void CancelIntraction()
    {
        progressBar.StopProgress();
    }

    public void Highlight()
    {
        // Debug.Log("Highlighted =" + ItemName);
        itemOutline.Show();
    }

    public void UnHighlight()
    {
        //Debug.Log("Un Highlighted =" + ItemName);
        itemOutline.Hide();
    }

    public float PlayAnimation(string clipName)
    {
        animationComp.Play(clipName);
        return animationComp.GetClip(clipName).length;
    }

    public void StopAnimation()
    {

    }

    public void LoadAllAnimations()
    {

    }

    public void PlayAudio(AudioClip audioClip)
    {
        if (audioSource != null)
        {
            audioSource.clip = audioClip;
            audioSource.Play();
        }
    }

    public void StopAudio()
    {
        if (audioSource != null)
        {
            audioSource.Stop();
        }
    }

    private void OnProgressComplete(object sender, EventArgs e)
    {
        OnTapped?.Invoke(this, new EventArgs());
        progressBar.StopProgress();
    }
}
