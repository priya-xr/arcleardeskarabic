﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectIntract : MonoBehaviour
{
    [SerializeField]
    private Camera mainCamera;
    private Transform currentHitObject;

    private IIntractable currentIntractObject;


    private void Update()
    {
        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                currentHitObject = hit.transform;
                IntractWithObject();
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            CancelIntract();
        }
    }

    private void IntractWithObject()
    {
        IIntractable intractable = currentHitObject.GetComponent<IIntractable>();

        if (currentIntractObject != null)
        {
            currentIntractObject.CancelIntraction();
        }

        if (intractable != null)
        {
            currentIntractObject = intractable;
            currentIntractObject.Intract();
        }
    }

    private void CancelIntract()
    {
        if (currentIntractObject != null)
        {
            currentIntractObject.CancelIntraction();
        }
    }
}
