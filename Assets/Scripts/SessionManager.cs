﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SessionManager : MonoBehaviour
{
    public static SessionManager instance;
    [SerializeField]
    private SessionDetailGet currentSession = null;
    public SessionDetailGet CurrentSession { get => currentSession; }
    public class OnConnectionStatusChangedArgs : EventArgs
    {
        public bool connected;
    }
    public EventHandler<OnConnectionStatusChangedArgs> OnConnectionStatusChanged;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

    }

    private void Start()
    {
        StartCoroutine(CheckConnectionStatus());
    }

    public void CreateSession(int employeeID, string platform, Action OnSessionCreated)
    {
        GetRecentSession(employeeID,
                        (SessionDetailGet args) =>
                        {
                            DateTime theTime = DateTime.Now;
                            string date = theTime.ToString("dd-MM-yyyy");
                            string time = System.DateTime.Now.ToLongTimeString();

                            if (args != null)
                            {
                                currentSession = args;
                                currentSession.evaluationScore = new EvaluationScore("", "", "", "");
                                currentSession.date = date;
                                currentSession.time = time;

                                OnSessionCreated?.Invoke();
                            }
                            else
                            {
                                currentSession = new SessionDetailGet(employeeID, platform);
                                currentSession.evaluationScore = new EvaluationScore("", "", "", "");
                                currentSession.date = date;
                                currentSession.time = time;

                                OnSessionCreated?.Invoke();
                            }
                        });

    }

    public void GetRecentSession(int employeeID, Action<SessionDetailGet> callback)
    {
        //return the employee details from last session
        SessionDetailsGet sessionDetails = null;
        string url = APILinks.getLatestSessionOfEmployee + employeeID.ToString();

        RestHandler.instance.Get(url,
                                APILinks.userName,
                                APILinks.password,
                                (RestGetCallBack args) =>
                                {
                                    //Debug.Log(args.data);
                                    sessionDetails = JsonUtility.FromJson<SessionDetailsGet>(args.data);

                                    if (sessionDetails.data.Count == 1)
                                    {
                                        callback?.Invoke(sessionDetails.data[0]);
                                    }
                                    else
                                    {
                                        callback?.Invoke(null);
                                    }
                                });
    }

    public void SetCurrentTrainingDetails(TrainingScore trainingScore)
    {
        CurrentSession.SetTrainingScore(trainingScore);
    }

    public void SetCurrentEvaluationDetails(EvaluationScore evaluationScore)
    {
        CurrentSession.SetEvaluationScore(evaluationScore);
    }

    public void IsEmployeeExists(int employeeID, Action<bool> isEmployeeExists)
    {
        //get all employees from rest and assign below
        string jsonString = "";

        RestHandler.instance.Get(APILinks.getAllEmployeeDetails,
                                 APILinks.userName,
                                 APILinks.password,
                                 ((RestGetCallBack args) =>
                                 {
                                     jsonString = args.data;
                                     EmployeeDetails employeeDetails = JsonUtility.FromJson<EmployeeDetails>(jsonString);//use tis for json

                                     bool isExists = false;
                                     for (int i = 0; i < employeeDetails.data.Count; i++)
                                     {
                                         if (employeeDetails.data[i].id == employeeID)
                                         {
                                             isExists = true;
                                         }
                                     }
                                     isEmployeeExists?.Invoke(isExists);
                                 }));
    }


    public bool IsTrainingComplete()
    {
        if (currentSession.trainingCompletionState == "COMPLETED")
            return true;
        else
            return false;
    }

    public int GetLastTrainingModuleIndex()
    {
        int i = int.Parse(currentSession.lastTrainingModuleIndex);
        return i;
    }

    private IEnumerator CheckConnectionStatus()
    {
        RestHandler.instance.Get(APILinks.connectionCheck, APILinks.userName, APILinks.password,
                                (RestGetCallBack args) =>
                                {
                                    if (!args.commandSuccessfullyCompleted)
                                    {
                                        OnConnectionStatusChanged?.Invoke(this, new OnConnectionStatusChangedArgs { connected = false });
                                    }
                                });

        yield return new WaitForSeconds(5f);

        StartCoroutine(CheckConnectionStatus());
    }

    private void SaveAllDetails()
    {

    }

    private void OnApplicationPause(bool paused)
    {
        //save here and exit
    }

}

[System.Serializable]
public class SessionDetailsGet
{
    public List<SessionDetailGet> data = new List<SessionDetailGet>();
}

[System.Serializable]
public class SessionDetailGet
{
    public int employeeID;
    public string platform;
    public string type;
    public string date;
    public string time;
    public string trainingCompletionState;//COMPLETED,INCOMPLETED
    public string lastTrainingModuleIndex;
    public TrainingScore trainingScore;
    public EvaluationScore evaluationScore;

    public SessionDetailGet(int employeeID, string platform)
    {
        this.employeeID = employeeID;
        this.platform = platform;
        this.type = "";
        this.date = "";
        this.time = "";
        this.trainingCompletionState = "INCOMPLETE";
        this.lastTrainingModuleIndex = "-1";

        this.trainingScore = new TrainingScore("", "", "", "", "0", " 0", " 0");
        this.evaluationScore = new EvaluationScore("", "", "", "");
    }

    public void SetTrainingScore(TrainingScore trainingScore)
    {
        this.trainingScore = trainingScore;
    }

    public void SetEvaluationScore(EvaluationScore evaluationScore)
    {
        this.evaluationScore = evaluationScore;
    }
}

[System.Serializable]
public class TrainingScore
{
    public string totalTimeTaken;
    public string module1TimeTaken;
    public string module2TimeTaken;
    public string module3TimeTaken;

    public string module1TimeSecs;
    public string module2TimeSecs;
    public string module3TimeSecs;

    public TrainingScore(string totalTimeTaken, string module1TimeTaken, string module2TimeTaken, string module3TimeTaken,
                         string module1TimeSecs, string module2TimeSecs, string module3TimeSecs)
    {
        this.totalTimeTaken = totalTimeTaken;
        this.module1TimeTaken = module1TimeTaken;
        this.module2TimeTaken = module2TimeTaken;
        this.module3TimeTaken = module3TimeTaken;

        this.module1TimeSecs = module1TimeSecs;
        this.module2TimeSecs = module2TimeSecs;
        this.module3TimeSecs = module3TimeSecs;
    }
}

[System.Serializable]
public class EvaluationScore
{
    public string totalScore;
    public string module1Score;
    public string module2Score;
    public string module3Score;

    public EvaluationScore(string totalScore, string module1Score, string module2Score, string module3Score)
    {
        this.totalScore = totalScore;
        this.module1Score = module1Score;
        this.module2Score = module2Score;
        this.module3Score = module3Score;
    }
}

[Serializable]
public class Employee
{
    public int id;
    public string employeeName;
    public string employeeEmail;
    public string dateCreated;
    public string dateUpdated;
    public string profilePicture;

    public Employee(int id, string employeeName, string employeeEmail, string dateCreated, string dateUpdated, string profilePicture)
    {
        this.id = id;
        this.employeeName = employeeName;
        this.employeeEmail = employeeEmail;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.profilePicture = profilePicture;
    }
}

[Serializable]
public class EmployeeDetails
{
    public List<Employee> data = new List<Employee>();

    public EmployeeDetails()
    {
        data = new List<Employee>();
    }
}

public class SessionDetailsPost
{
    public int employeeID;
    public string platform;
    public string type;
    public string date;
    public string time;
    public string trainingCompletionState;
    public int lastTrainingModuleIndex;
    public string totalTimeTaken;
    public string module1TimeTaken;
    public string module2TimeTaken;
    public string module3TimeTaken;
    public string totalScore;
    public string module1Score;
    public string module2Score;
    public string module3Score;
    public string module1TimeSecs;
    public string module2TimeSecs;
    public string module3TimeSecs;

    public SessionDetailsPost(SessionDetailGet sessionDetailsGet)
    {
        this.employeeID = sessionDetailsGet.employeeID;
        this.platform = sessionDetailsGet.platform;
        this.type = sessionDetailsGet.type;

        this.date = sessionDetailsGet.date;
        this.time = sessionDetailsGet.time;
        this.trainingCompletionState = sessionDetailsGet.trainingCompletionState;
        this.lastTrainingModuleIndex = int.Parse(sessionDetailsGet.lastTrainingModuleIndex);

        this.totalTimeTaken = sessionDetailsGet.trainingScore.totalTimeTaken;
        this.module1TimeTaken = sessionDetailsGet.trainingScore.module1TimeTaken;
        this.module2TimeTaken = sessionDetailsGet.trainingScore.module2TimeTaken;
        this.module3TimeTaken = sessionDetailsGet.trainingScore.module3TimeTaken;

        this.module1TimeSecs = sessionDetailsGet.trainingScore.module1TimeSecs;
        this.module2TimeSecs = sessionDetailsGet.trainingScore.module2TimeSecs;
        this.module3TimeSecs = sessionDetailsGet.trainingScore.module3TimeSecs;

        this.totalScore = sessionDetailsGet.evaluationScore.totalScore;
        this.module1Score = sessionDetailsGet.evaluationScore.module1Score;
        this.module2Score = sessionDetailsGet.evaluationScore.module2Score;
        this.module3Score = sessionDetailsGet.evaluationScore.module3Score;

    }
}