﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeftAlign : MonoBehaviour
{
    [HideInInspector]
    public LanguageManager languageManager;
    // Update is called once per frame

   void Start()
    {
        languageManager = LanguageManager.instance;
    }

    void Update()
    {
        if (languageManager.CurrentLaguage == Languages.Arabic)
            gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
        else
            gameObject.GetComponent<Text>().alignment = TextAnchor.MiddleRight;
    }
}
