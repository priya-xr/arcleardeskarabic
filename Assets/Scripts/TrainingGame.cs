﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class TrainingGame : TrainingMiniGame
{
    [SerializeField]
    private TrainingGameUI trainingGameUI;

    [SerializeField]
    private TrainingModule[] allModules;
    private int currentModuleIndex;
    private int currentTaskIndex;
    private int currentSubTaskIndex;
    [SerializeField]
    private TrainingTask activeSubTask;
    [SerializeField]
    private Timer timer;
    private bool isGameStarted;

    [Header("AR")]
    [SerializeField]
    private ARFoundationCalibration aRFoundationCalibration;
    [SerializeField]
    private TouchControl touchControl;
    [SerializeField]
    private Transform arRoot;
    [SerializeField]
    private Transform dummy;
    [SerializeField]
    private Transform actual;
    [SerializeField]
    private LookAt lookAt;

    public GameObject pinch;
    public GameObject zoom;

    public int CurrentModuleIndex { get => currentModuleIndex; }
    public int CurrentTaskIndex { get => currentTaskIndex; }
    public int CurrentSubTaskIndex { get => currentSubTaskIndex; }
    public bool IsGameStarted { get => isGameStarted; }

    [HideInInspector]
    public SessionManager sessionManager;
    [HideInInspector]
    public LevelManager levelManager;



    private SessionDetailGet currentSession;


    private void Awake()
    {

    }

    private void Start()
    {
        languageManager = LanguageManager.instance;
        levelManager = LevelManager.instance;
        sessionManager = SessionManager.instance;

        trainingGameUI.ShowPanel("CalibrationPanel", false);
        trainingGameUI.EnableCalibrationCanvas(true);
        StartCalibration();
        //EnterGame();
    }

    public override void EnterGame()
    {
        if (GlobalValues.instance.TrainingState == TRAININGSTATES.RESUME)
        {
            currentSession = sessionManager.CurrentSession;
            HandleResume();//load here the module index
        }
        else if (GlobalValues.instance.TrainingState == TRAININGSTATES.NEWSESSION)
        {
            sessionManager.CurrentSession.trainingScore = new TrainingScore("0", "0", "0", "0", "0", "0", "0");
            currentSession = sessionManager.CurrentSession;
            InitModule(0);
        }
        isGameStarted = true;
        base.EnterGame();
    }

    private void HandleResume()
    {
        int index = 0;
        int.TryParse(currentSession.lastTrainingModuleIndex, out index);

        if (index == 0)
        {
            currentSession = sessionManager.CurrentSession;
            double timeTaken = 0;
            double.TryParse(currentSession.trainingScore.module1TimeSecs, out timeTaken);
            allModules[0].timeTaken = timeTaken;


            InitModule(1);
            CompleteTasksInModule(new int[] { 0 });
        }
        else if (index == 1)
        {
            currentSession = sessionManager.CurrentSession;

            double timeTaken = 0;

            double.TryParse(currentSession.trainingScore.module1TimeSecs, out timeTaken);
            allModules[0].timeTaken = timeTaken;

            double.TryParse(currentSession.trainingScore.module2TimeSecs, out timeTaken);
            allModules[1].timeTaken = timeTaken;

            InitModule(2);
            CompleteTasksInModule(new int[] { 0, 1 });
        }
    }

    public void InitModule(int moduleIndex)
    {
        currentModuleIndex = moduleIndex;
        currentTaskIndex = 0;
        currentSubTaskIndex = 0;

        Content[] moduleContent = new Content[3];
        TrainingModule module = allModules[moduleIndex];
        moduleContent[0] = module.moduleID;
        moduleContent[1] = module.moduleName;
        moduleContent[2] = module.description;

        string intractable = "false";
        if (currentModuleIndex >= allModules.Length - 1 || currentSession.trainingCompletionState.Equals("INCOMPLETE"))
        {
            intractable = "false";
        }
        else
        {
            intractable = "true";
        }
        object[] objects = new object[4];
        objects[0] = moduleContent[0];
        objects[1] = moduleContent[1];
        objects[2] = moduleContent[2];
        objects[3] = intractable;

        trainingGameUI.ShowPanel("ModuleStartPanel", true);
        trainingGameUI.UpdatePanelDetails("ModuleStartPanel", objects);
    }

    public void StartModule(int moduleIndex)
    {
        currentModuleIndex = moduleIndex;
        currentTaskIndex = 0;
        currentSubTaskIndex = 0;

        TaskData subTaskData = GetSubTaskDetails(CurrentModuleIndex, CurrentTaskIndex, CurrentSubTaskIndex);
        //trainingGameUI.ShowPanel("TaskPanel", true);
        //StartSubTask(GetTask(currentModuleIndex, currentTaskIndex), subTaskData);
        InitSubTask(GetTask(currentModuleIndex, currentTaskIndex), subTaskData);

        //like infinite time 
        timer.StopTimer();
        timer.StartTimer(9000000000);
    }

    private void InitSubTask(ModuleTask task, TaskData subTaskData)
    {
        Content moduleName = allModules[currentModuleIndex].moduleName;
        Content subModuleName = task.taskName;
        Content desc = task.startContent;

        object[] args = new object[3] { moduleName, subModuleName, desc };

        trainingGameUI.UpdatePanelDetails("SubModuleStartPanel", args);
        trainingGameUI.ShowPanel("SubModuleStartPanel", true);
    }

    public void StartSubTask()
    {
        TaskData subTaskData = GetSubTaskDetails(CurrentModuleIndex, CurrentTaskIndex, CurrentSubTaskIndex);
        StartSubTask(GetTask(currentModuleIndex, currentTaskIndex), subTaskData);
    }

    public void StartSubTask(ModuleTask task, TaskData subTaskData)
    {
        trainingGameUI.ShowPanel("TaskPanel", true);

        if (activeSubTask != null)
        {
            activeSubTask.OnTaskFinished -= OnSubTaskComplete;
        }
        activeSubTask = subTaskData.task;
        activeSubTask.OnTaskFinished += OnSubTaskComplete;
        activeSubTask.StartTask();

        Content[] contents = new Content[4];
        contents[0] = task.taskID;
        contents[1] = allModules[currentModuleIndex].moduleName;
        contents[2] = subTaskData.header;
        contents[3] = subTaskData.description;

        trainingGameUI.UpdateTaskSubPanel("TaskInfoPanel", contents);
        offScreenIndicator.SetTarget(activeSubTask.TaskTransform);

        object[] args = new object[1];
        args[0] = activeSubTask.TaskItem.ItemID;

        trainingGameUI.UpdateTaskSubPanel("OffScreenIndicatorPanel", args);
        trainingGameUI.ShowTaskSubPanel("OffScreenIndicatorPanel");
    }

    private void OnSubTaskComplete(object sender, EventArgs e)
    {
        StartCoroutine(HandleOnSubTaskComplete());
    }

    private IEnumerator HandleOnSubTaskComplete()
    {
        int numberOfSubTasks = GetSubTasksCount(CurrentModuleIndex, CurrentTaskIndex);
        int numberOfTasks = GetTasksCount(CurrentModuleIndex);

        if (CurrentSubTaskIndex == numberOfSubTasks - 1)
        {
            //all subtasks completed
            //go to next task if theres next task or else show module finish screen
            AudioManager.instance.PlaySound("CompletedSound", false);

            Content ModuleName = allModules[currentModuleIndex].moduleName;
            Content subModuleName = allModules[currentModuleIndex].moduleTasks[currentTaskIndex].taskName;
            Content finish = allModules[currentModuleIndex].moduleTasks[currentTaskIndex].finishContent;

            trainingGameUI.UpdatePanelDetails("SubModuleFinishPanel", new object[] { ModuleName, subModuleName, finish });
            trainingGameUI.ShowPanel("SubModuleFinishPanel", false);
            trainingGameUI.HidePanel("TaskPanel");
        }
        else
        {
            //go to next sub task
            currentSubTaskIndex++;
            TaskData subTaskData = GetSubTaskDetails(CurrentModuleIndex, CurrentTaskIndex, CurrentSubTaskIndex);
            StartSubTask(GetTask(currentModuleIndex, currentTaskIndex), subTaskData);
        }

        yield return null;
    }

    public void SubModuleFinishPanelConfirmation()
    {
        int numberOfTasks = GetTasksCount(CurrentModuleIndex);

        trainingGameUI.HidePanel("SubModuleFinishPanel");
        trainingGameUI.ShowPanel("TaskPanel", false);

        if (CurrentTaskIndex == numberOfTasks - 1)
        {
            //end of tasks in current module
            //so module completed 
            //show finish ui 
            allModules[currentModuleIndex].timeTaken = timer.CurrentTime;
            timer.StopTimer();

            Content[] contents = new Content[3];
            contents[0] = allModules[currentModuleIndex].moduleID;
            contents[1] = allModules[currentModuleIndex].moduleFinish;
            contents[2] = allModules[currentModuleIndex].moduleName;
            string canProceedNext = currentModuleIndex == GetModulesCount() - 1 ? "false" : "true";
            string canSubmit = currentModuleIndex == GetModulesCount() - 1 ? "true" : "false";

            trainingGameUI.UpdatePanelDetails("ModuleFinishPanel", new object[] { contents[0], contents[1], canProceedNext, canSubmit, contents[2] });
            trainingGameUI.ShowPanel("ModuleFinishPanel", true);
            trainingGameUI.HideTaskSubPanel("OffScreenIndicatorPanel");

            //store in session
            OnModuleComplete();
        }
        else
        {
            //proceed to next task
            currentTaskIndex++;
            currentSubTaskIndex = 0;
            TaskData subTaskData = GetSubTaskDetails(CurrentModuleIndex, CurrentTaskIndex, CurrentSubTaskIndex);
            InitSubTask(GetTask(currentModuleIndex, currentTaskIndex), subTaskData);
        }
    }

    private void OnModuleComplete()
    {
        currentSession.lastTrainingModuleIndex = CurrentModuleIndex.ToString();
        //if all modules completed store it
        if (CurrentModuleIndex == allModules.Length - 1)
        {
            currentSession.trainingCompletionState = "COMPLETED";
        }
    }

    public void DisplayScore()
    {
        TrainingScore trainingScore = GetScore();
        sessionManager.SetCurrentTrainingDetails(trainingScore);

        object[] args = new object[5];
        args[0] = sessionManager.CurrentSession.employeeID.ToString();
        args[1] = trainingScore.totalTimeTaken;
        args[2] = trainingScore.module1TimeTaken;
        args[3] = trainingScore.module2TimeTaken;
        args[4] = trainingScore.module3TimeTaken;

        trainingGameUI.UpdatePanelDetails("TrainingScorePanel", args);
        trainingGameUI.ShowPanel("TrainingScorePanel", true);
    }

    private TrainingScore GetScore()
    {
        double m1TimeTaken = allModules[0].timeTaken;
        double m2TimeTaken = allModules[1].timeTaken;
        double m3TimeTaken = allModules[2].timeTaken;

        double totalTimeTaken = m1TimeTaken + m2TimeTaken + m3TimeTaken;

        string[] values = new string[5];

        values[0] = sessionManager.CurrentSession.employeeID.ToString();
        values[1] = SecsToMinutes(totalTimeTaken);
        values[2] = SecsToMinutes(m1TimeTaken);
        values[3] = SecsToMinutes(m2TimeTaken);
        values[4] = SecsToMinutes(m3TimeTaken);

        TrainingScore trainingScore = new TrainingScore(values[1], values[2], values[3], values[4],
                                                        m1TimeTaken.ToString(), m2TimeTaken.ToString(), m3TimeTaken.ToString());

        return trainingScore;
    }

    private string SecsToMinutes(double seconds)
    {
        int min = Mathf.FloorToInt((float)seconds / 60f);
        int sec = Mathf.FloorToInt((float)seconds % 60f);

        return min.ToString("00") + "min " + sec.ToString("00") + "sec";
    }

    public void SaveDetailsToServer()
    {
        //push to server here
        currentSession.trainingScore = GetScore();
        currentSession.type = "Training";

        SessionDetailsPost session = new SessionDetailsPost(currentSession);
        string jsonString = JsonUtility.ToJson(session);
        RestHandler.instance.Post(APILinks.postSessionOfEmployee, jsonString, APILinks.userName
                                , APILinks.password, (RestGetCallBack args) => { });

    }

    public void ExitToModuleSelection()
    {
        GlobalValues.instance.BaseStates = BASESTATES.MODULESELECTION;
        levelManager.LoadScene("Start");
    }

    public void SkipCurrentModule()
    {
        int[] modules = new int[currentModuleIndex + 1];
        for (int i = 0; i < modules.Length; i++)
        {
            modules[i] = i;
        }
        currentModuleIndex += 1;
        InitModule(currentModuleIndex);
        CompleteTasksInModule(modules);
    }

    private void CompleteTasksInModule(int[] moduleIndexes)
    {
        StartCoroutine(CompleteTasks(moduleIndexes));
    }

    private IEnumerator CompleteTasks(int[] moduleIndexes)
    {
        //yield return new WaitForSeconds(10f);
        //show loading screen
        trainingGameUI.ShowPanel("LoadingPanel", false);

        for (int k = 0; k < moduleIndexes.Length; k++)
        {
            int moduleIndex = moduleIndexes[k];

            for (int i = 0; i < allModules[moduleIndex].moduleTasks.Length; i++)
            {
                for (int j = 0; j < allModules[moduleIndex].moduleTasks[i].subTasks.Length; j++)
                {
                    float time = allModules[moduleIndex].moduleTasks[i].subTasks[j].task.CompleteTask();

                    yield return new WaitForSeconds(time + .1f);
                }
            }
        }

        trainingGameUI.HidePanel("LoadingPanel");
        //hide loading screen
        yield return null;
    }


    public void RedoCurrentModule()
    {
        ResetModule(currentModuleIndex);
        StartModule(currentModuleIndex);
    }

    private ModuleTask GetTask(int moduleIndex, int taskIndex)
    {
        ModuleTask Task = allModules[moduleIndex].moduleTasks[taskIndex];
        return Task;
    }

    private Task GetSubTask(int moduleIndex, int taskIndex, int subTaskIndex)
    {
        Task subTask = allModules[moduleIndex].moduleTasks[taskIndex].subTasks[subTaskIndex].task;
        return subTask;
    }

    private TaskData GetSubTaskDetails(int moduleIndex, int taskIndex, int subTaskIndex)
    {
        TaskData subTaskData = allModules[moduleIndex].moduleTasks[taskIndex].subTasks[subTaskIndex];
        return subTaskData;
    }

    private void ResetModule(int moduleIndex)
    {
        for (int i = 0; i < allModules[moduleIndex].moduleTasks.Length; i++)
        {
            for (int j = 0; j < allModules[moduleIndex].moduleTasks[i].subTasks.Length; j++)
            {
                allModules[moduleIndex].moduleTasks[i].subTasks[j].task.ResetTask();
                allModules[moduleIndex].timeTaken = 0;
            }
        }
    }

    private int GetModulesCount()
    {
        return allModules.Length;
    }

    private int GetTasksCount(int moduleIndex)
    {
        return allModules[moduleIndex].moduleTasks.Length;
    }

    private int GetSubTasksCount(int moduleIndex, int taskIndex)
    {
        return allModules[moduleIndex].moduleTasks[taskIndex].subTasks.Length;
    }

    public override void UpdateGame()
    {

    }

    public override void FinishGame()
    {
        DisplayScore();
        isGameStarted = false;
        base.FinishGame();
    }

    public override void ExitGame()
    {
        base.ExitGame();
    }

    public override void ResetGame()
    {
        base.ResetGame();
    }

    public override void PauseGame()
    {
        timer.PauseTimer();
        base.PauseGame();
    }

    public override void ResumeGame()
    {
        timer.ResumeTimer();
        base.ResumeGame();
    }

    public void ChangeLanguage(Languages language)
    {
        languageManager.ChangeLanguage(language);
    }

    public void ToggleLanguage()
    {
        languageManager.ToggleLanguage();
    }

    public void SaveGoHome()
    {
        SaveDetailsToServer();
        GlobalValues.instance.BaseStates = BASESTATES.BEGIN;
        levelManager.LoadScene("Start");
    }

    public void GoHome()
    {
        GlobalValues.instance.BaseStates = BASESTATES.BEGIN;
        levelManager.LoadScene("Start");
    }

    //----------
    //Calibration
    public void StartCalibration()
    {
        if (isGameStarted)
        {
            PauseGame();
        }

        aRFoundationCalibration.OnPlaneFound += OnPlaneFound;
        aRFoundationCalibration.OnPlaneNotFound += OnPlaneNotFound;
        touchControl.WhenSwiped += RotateARRoot;
        touchControl.WhenPinched += ScaleARRoot;

        dummy.gameObject.SetActive(false);
        actual.gameObject.SetActive(false);

        aRFoundationCalibration.StartCalibration();
        touchControl.enabled = true;
        lookAt.enabled = true;
    }

    private void StopCalibration()
    {
        aRFoundationCalibration.OnPlaneFound -= OnPlaneFound;
        aRFoundationCalibration.OnPlaneNotFound -= OnPlaneNotFound;
        touchControl.WhenSwiped -= RotateARRoot;
        touchControl.WhenPinched -= ScaleARRoot;

        aRFoundationCalibration.StopCalibration();
        touchControl.enabled = false;
        lookAt.enabled = false;
    }

    private void OnPlaneFound(object sender, ARFoundationCalibration.OnPlaneFoundEventArgs args)
    {
        arRoot.position = args.hitPosition;

        //arRoot.rotation = args.hitRotation;
        dummy.gameObject.SetActive(true);
        pinch.SetActive(true);
        zoom.SetActive(true);
        //button.SetActive(true);

        //set intractable of placebtn set to true
        string s = "true";
        trainingGameUI.UpdatePanelDetails("CalibrationPanel", new object[1] { s });
    }

    private void OnPlaneNotFound(object sender, EventArgs args)
    {
        dummy.gameObject.SetActive(false);
        pinch.SetActive(false);
        zoom.SetActive(false);
        //set intractable of placebtn set to false
        string s = "false";
        trainingGameUI.UpdatePanelDetails("CalibrationPanel", new object[1] { s });
    }

    public void Place()
    {
        trainingGameUI.HidePanel("MainMenuPanel");
        dummy.gameObject.SetActive(false);
        pinch.SetActive(false);
        zoom.SetActive(false);
        //button.gameObject.SetActive(false);
        actual.gameObject.SetActive(true);
        StopCalibration();
        if (!isGameStarted)
        {
            EnterGame();

        }
        else
        {
            //ResumeGame();
        }

        trainingGameUI.EnableCalibrationCanvas(false);
        trainingGameUI.HidePanel("CalibrationPanel");
    }

    public void RotateARRoot(Vector3 delta)
    {
        if (Input.touchCount != 1)
            return;

        lookAt.offset += delta.x * -.1f;
        //arRoot.transform.Rotate(new Vector3(0, delta.x * -.1f, 0));
    }

    public void ScaleARRoot(float delta)
    {
        float min = .1f;
        float max = .25f;
        float currentScale = arRoot.transform.localScale.x + (delta * .0005f);

        currentScale = Mathf.Clamp(currentScale, min, max);
        arRoot.transform.localScale = Vector3.one * currentScale;
    }

}


[System.Serializable]
public class TrainingModule
{
    public string module;
    public Content moduleID;
    public Content moduleName;
    public Content description;
    public Content moduleFinish;
    public ModuleTask[] moduleTasks;
    public double timeTaken;
}

[System.Serializable]
public class ModuleTask
{
    public string taskNote;
    public Content taskID;
    public Content taskName;
    public Content startContent;
    public Content finishContent;
    public TaskData[] subTasks;
}

[System.Serializable]
public class TaskData
{
    public TrainingTask task;
    public Content header;
    public Content description;
}