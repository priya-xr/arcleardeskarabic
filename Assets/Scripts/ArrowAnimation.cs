﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAnimation : MonoBehaviour
{
    public Animator Left;
    public Animator Right;
    public Animator Top;
    public Animator Down;

    //public GameObject pinch;
   // public GameObject zoom;

    public void LeftArrow()
    {
        Left.SetTrigger("left");
    }

    public void RightArrow()
    {
        Right.SetTrigger("right");
    }

    public void TopArrow()
    {
        Top.SetTrigger("top");
    }

    public void DownArrow()
    {
        Down.SetTrigger("down");
    }

    public void ChangeIcons()
    {
        Left.gameObject.SetActive(false);
        Right.gameObject.SetActive(false);
        Top.gameObject.SetActive(false);
        Down.gameObject.SetActive(false);

        //pinch.SetActive(true);
        //zoom.SetActive(true);

    }

}
