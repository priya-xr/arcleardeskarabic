﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class APILinks
{
    public static string userName = "admin";
    public static string password = "admin!@#";
    public static string getAllEmployeeDetails = "https://louvreapp.xrlabs.cloud/api/get-all-employees";
    public static string getLatestSessionOfEmployee = "https://louvreapp.xrlabs.cloud/api/get-latest-session/";
    public static string postSessionOfEmployee = "https://louvreapp.xrlabs.cloud/api/add-session";
    public static string connectionCheck="https://louvreapp.xrlabs.cloud/api/connection-check";

}
