﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TapKeyTask : EvaluateTask
{
    [SerializeField]
    private Game miniGame;
    [SerializeField]
    private TapItem itemToTap;
    [SerializeField]
    private Item[] allKeys;
    [SerializeField]
    private Item thisKey;
    [SerializeField]
    private GameObject dropFakeItem;
    [SerializeField]
    private float intractionTime;

    [SerializeField]
    private AnimationClip animationToPlay;
    [SerializeField]
    private AudioClip audioClip;

    [SerializeField]
    private AnimationClip completeAnimation;


    public override void StartTask()
    {
        itemToTap.OnTapped += OnTappedOnItem;
        itemToTap.IntractionTime = intractionTime;
        itemToTap.IsIntratable = true;

        base.StartTask();
    }

    private void Update()
    {

    }

    public override void FinishTask()
    {
        base.FinishTask();
    }

    public override void ResetTask()
    {
        IsTaskFinished = false;
        base.ResetTask();
    }

    private void OnTappedOnItem(object sender, EventArgs e)
    {
        if (miniGame.Inventory.ItemInHand == null)
        {
            return;
        }

        if (thisKey == miniGame.Inventory.ItemInHand)
        {
            dropFakeItem.gameObject.SetActive(true);
            itemToTap.OnTapped -= OnTappedOnItem;

            miniGame.Inventory.RemoveItem(miniGame.Inventory.ItemInHand);

            float animTime = itemToTap.PlayAnimation(animationToPlay.name);
            if (audioClip != null)
            {
                itemToTap.PlayAudio(audioClip);
            }

            StartCoroutine(WaitAndFinish(animTime));
        }
        else if (!IsSuitableKey(miniGame.Inventory.ItemInHand))
        {
            miniGame.Inventory.ItemInHand.Show();
            miniGame.Inventory.RemoveItem(miniGame.Inventory.ItemInHand);
        }
    }

    private bool IsSuitableKey(Item key)
    {
        for (int i = 0; i < allKeys.Length; i++)
        {
            if (allKeys[i] == key)
            {
                return true;
            }
        }
        return false;
    }
    private IEnumerator WaitAndFinish(float time)
    {
        yield return new WaitForSeconds(time);
        FinishTask();
    }

    public override float CompleteTask()
    {
        float t = 0;

        if (completeAnimation)
            t = itemToTap.PlayAnimation(completeAnimation.name);

        dropFakeItem.SetActive(true);
        thisKey.Hide();

        return t;
    }
}
