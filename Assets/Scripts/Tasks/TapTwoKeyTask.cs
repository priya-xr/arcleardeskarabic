﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TapTwoKeyTask : EvaluateTask
{
    [SerializeField]
    private Game game;
    [SerializeField]
    private TapItem itemToTap;
    [SerializeField]
    private Item[] allKeys;
    [SerializeField]
    private Item keyOne;
    [SerializeField]
    private Item keytwo;
    [SerializeField]
    private GameObject dropFakeItem;
    [SerializeField]
    private float intractionTime;

    [SerializeField]
    private AnimationClip animationToPlay;


    public override void StartTask()
    {
        itemToTap.OnTapped += OnTappedOnItem;
        itemToTap.IntractionTime = intractionTime;
        itemToTap.IsIntratable = true;

        base.StartTask();
    }

    private void Update()
    {

    }

    public override void FinishTask()
    {
        base.FinishTask();
    }

    public override void ResetTask()
    {
        IsTaskFinished = false;

        base.ResetTask();
    }

    private void OnTappedOnItem(object sender, EventArgs e)
    {
        if (game.Inventory.ItemInHand == null)
            return;


        if ((keyOne == game.Inventory.ItemInHand) || (keytwo == game.Inventory.ItemInHand))
        {
            dropFakeItem.gameObject.SetActive(true);
            itemToTap.OnTapped -= OnTappedOnItem;

            game.Inventory.RemoveItem(game.Inventory.ItemInHand);

            float animTime = itemToTap.PlayAnimation(animationToPlay.name);
            StartCoroutine(WaitAndFinish(animTime));
        }
        else if (!IsSuitableKey(game.Inventory.ItemInHand))
        {
            game.Inventory.ItemInHand.Show();
            game.Inventory.RemoveItem(game.Inventory.ItemInHand);
        }

    }

    private bool IsSuitableKey(Item key)
    {
        for (int i = 0; i < allKeys.Length; i++)
        {
            if (allKeys[i] == key)
            {
                return true;
            }
        }
        return false;
    }
    private IEnumerator WaitAndFinish(float time)
    {
        yield return new WaitForSeconds(time);
        FinishTask();
    }

    public override float CompleteTask()
    {
        keyOne.Hide();
        return 0;
    }

}
