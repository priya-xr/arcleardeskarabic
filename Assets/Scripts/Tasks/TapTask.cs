﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class TapTask : TrainingTask
{
    [SerializeField]
    private TapItem itemToTap;
    [SerializeField]
    private float intractionTime;

    [SerializeField]
    private AnimationClip animationToPlay;
    [SerializeField]
    private AnimationClip resetAnimation;
    [SerializeField]
    private AnimationClip completeAnimation;


    [SerializeField]
    private AudioClip audioToPlay;

    public override void StartTask()
    {
        itemToTap.OnTapped += OnTappedOnItem;
        itemToTap.Highlight();

        itemToTap.IntractionTime = intractionTime;
        itemToTap.IsIntratable = true;

        base.StartTask();
    }

    private void Update()
    {
        //if (IsTaskFinished)
        //  FinishTask();
    }

    public override void FinishTask()
    {
        IsTaskFinished = true;
        itemToTap.IsIntratable = false;
        base.FinishTask();
    }

    public override void ResetTask()
    {
        IsTaskFinished = false;
        if (resetAnimation)
        {
            itemToTap.PlayAnimation(resetAnimation.name);
        }
        base.ResetTask();
    }

    private void OnTappedOnItem(object sender, EventArgs e)
    {
        itemToTap.OnTapped -= OnTappedOnItem;
        itemToTap.UnHighlight();

        float animTime = 0;
        if (animationToPlay)
        {
            animTime = itemToTap.PlayAnimation(animationToPlay.name);
        }

        if (audioToPlay)
        {
            itemToTap.PlayAudio(audioToPlay);
        }

        StartCoroutine(WaitAndFinish(animTime));
    }

    private IEnumerator WaitAndFinish(float time)
    {
        yield return new WaitForSeconds(time);
        FinishTask();
    }

    public override float CompleteTask()
    {
        if (completeAnimation)
        {
            float f = itemToTap.PlayAnimation(completeAnimation.name);
            return f;
        }
        else
        {
            return 0;
        }

    }
}
