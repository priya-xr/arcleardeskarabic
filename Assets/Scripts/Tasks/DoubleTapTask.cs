﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class DoubleTapTask : Task
{
    [SerializeField]
    private TapItem firstTapItem;
    [SerializeField]
    private TapItem secondTapItem;
    [SerializeField]
    private float intractionTime;

    [SerializeField]
    private bool isFirstItemTapped;

    [SerializeField]
    private AnimationClip animationClip;
    [SerializeField]
    private AnimationClip completeAnimationClip;

    public override void StartTask()
    {
        firstTapItem.OnTapped += OnTappedOnFirstItem;
        firstTapItem.IsIntratable = true;

        base.StartTask();
    }

    public override void FinishTask()
    {
        IsTaskFinished = true;
        firstTapItem.IsIntratable = false;
        base.FinishTask();
    }

    public override void ResetTask()
    {
        IsTaskFinished = false;
        base.ResetTask();
    }

    private void OnTappedOnFirstItem(object sender, EventArgs e)
    {
        firstTapItem.OnTapped -= OnTappedOnFirstItem;
        secondTapItem.OnTapped += OnTappedOnSecondItem;
        secondTapItem.IsIntratable = true;

        firstTapItem.IsIntratable = false;
        isFirstItemTapped = true;
    }

    private void OnTappedOnSecondItem(object sender, EventArgs e)
    {
        secondTapItem.OnTapped -= OnTappedOnSecondItem;
        secondTapItem.IsIntratable = false;

        StartCoroutine(WaitAndFinish(secondTapItem.PlayAnimation(animationClip.name)));
    }

    private IEnumerator WaitAndFinish(float time)
    {
        yield return new WaitForSeconds(time);
        FinishTask();
    }

    public override float CompleteTask()
    {
        float t = 0;
        if (completeAnimationClip)
            t = secondTapItem.PlayAnimation(completeAnimationClip.name);

        return t;
    }
}
