﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class DoubleTapLaptopTask : Task
{
    [SerializeField]
    private TapItem firstTapItem;
    [SerializeField]
    private TapItem secondTapItem;
    [SerializeField]
    private float intractionTime;

    [SerializeField]
    private bool isFirstItemTapped;

    [SerializeField]
    private AnimationClip animationClip;
    [SerializeField]
    private AnimationClip completeAnimationClip;

    [SerializeField]
    private AudioClip audioClip;

    [SerializeField]
    private GameObject lockedScreen;
    [SerializeField]
    private GameObject unLockedScreen;

    public override void StartTask()
    {
        firstTapItem.OnTapped += OnTappedOnFirstItem;
        firstTapItem.IsIntratable = true;

        base.StartTask();
    }

    public override void FinishTask()
    {
        IsTaskFinished = true;
        firstTapItem.IsIntratable = false;
        base.FinishTask();
    }

    public override void ResetTask()
    {
        IsTaskFinished = false;
        lockedScreen.SetActive(false);
        unLockedScreen.SetActive(true);
        base.ResetTask();
    }

    private void OnTappedOnFirstItem(object sender, EventArgs e)
    {
        firstTapItem.OnTapped -= OnTappedOnFirstItem;
        secondTapItem.OnTapped += OnTappedOnSecondItem;
        secondTapItem.IsIntratable = true;

        if (audioClip)
        {
            firstTapItem.PlayAudio(audioClip);
        }

        firstTapItem.IsIntratable = false;

        lockedScreen.SetActive(true);
        unLockedScreen.SetActive(false);

        isFirstItemTapped = true;
    }

    private void OnTappedOnSecondItem(object sender, EventArgs e)
    {
        secondTapItem.OnTapped -= OnTappedOnSecondItem;
        secondTapItem.IsIntratable = false;

        StartCoroutine(WaitAndFinish(secondTapItem.PlayAnimation(animationClip.name)));
    }

    private IEnumerator WaitAndFinish(float time)
    {
        yield return new WaitForSeconds(time);
        FinishTask();
    }

    public override float CompleteTask()
    {
        if (completeAnimationClip)
            secondTapItem.PlayAnimation(completeAnimationClip.name);

        lockedScreen.SetActive(true);
        unLockedScreen.SetActive(false);

        return 0f;
    }
}
