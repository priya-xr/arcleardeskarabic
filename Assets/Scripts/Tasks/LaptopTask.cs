﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class LaptopTask : TrainingTask
{
    [SerializeField]
    private TapItem itemToTap;
    [SerializeField]
    private float intractionTime;

    [SerializeField]
    private GameObject lockedScreen;

    [SerializeField]
    private GameObject unLockedScreen;

    [SerializeField]
    private AudioClip audioToPlay;


    public override void StartTask()
    {
        itemToTap.OnTapped += OnTappedOnItem;
        itemToTap.Highlight();

        itemToTap.IntractionTime = intractionTime;
        itemToTap.IsIntratable = true;

        base.StartTask();
    }

    private void Update()
    {
        //if (IsTaskFinished)
        //  FinishTask();
    }

    public override void FinishTask()
    {
        IsTaskFinished = true;
        itemToTap.IsIntratable = false;
        base.FinishTask();
    }

    public override void ResetTask()
    {
        IsTaskFinished = false;
        lockedScreen.SetActive(false);
        unLockedScreen.SetActive(true);
        base.ResetTask();
    }

    private void OnTappedOnItem(object sender, EventArgs e)
    {
        itemToTap.OnTapped -= OnTappedOnItem;
        itemToTap.UnHighlight();

        float animTime = 0;
        lockedScreen.SetActive(true);
        unLockedScreen.SetActive(false);


        if (audioToPlay != null)
            itemToTap.PlayAudio(audioToPlay);

        StartCoroutine(WaitAndFinish(animTime));
    }

    private IEnumerator WaitAndFinish(float time)
    {
        yield return new WaitForSeconds(time);
        FinishTask();
    }
}
