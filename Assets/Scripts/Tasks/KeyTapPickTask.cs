﻿using System;
using UnityEngine;

public class KeyTapPickTask : TrainingTask
{
    [SerializeField]
    private TrainingMiniGame miniGame;
    [SerializeField]
    private TapItem itemToPick;
    [SerializeField]
    private float intractionTime;

    public override void StartTask()
    {
        itemToPick.OnTapped += OnTappedOnItem;
        itemToPick.Highlight();
        itemToPick.IntractionTime = intractionTime;
        itemToPick.IsIntratable = true;

        base.StartTask();
    }

    private void Update()
    {
        //if (IsTaskFinished)
        //  FinishTask();
    }

    public override void FinishTask()
    {
        IsTaskFinished = true;
        itemToPick.IsIntratable = false;
        base.FinishTask();
    }

    public override void ResetTask()
    {
        IsTaskFinished = false;
        itemToPick.Hide();

        base.ResetTask();
    }

    private void OnTappedOnItem(object sender, EventArgs e)
    {
        itemToPick.OnTapped -= OnTappedOnItem;
        itemToPick.UnHighlight();

        //add to inventory
        miniGame.Inventory.AddItem(itemToPick);
        itemToPick.Hide();
        FinishTask();
    }
}
