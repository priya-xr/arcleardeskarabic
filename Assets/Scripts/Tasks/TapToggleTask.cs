﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class TapToggleTask : Task
{
    [SerializeField]
    private TapItem itemToTap;
    [SerializeField]
    private float intractionTime;

    [SerializeField]
    private AnimationClip animationToPlay;

    [SerializeField]
    private AnimationClip resetAnimation;

    [SerializeField]
    private AudioClip audioClip1;
    [SerializeField]
    private AudioClip audioClip2;


    public override void StartTask()
    {
        itemToTap.OnTapped += OnTappedOnItem;
        itemToTap.IntractionTime = intractionTime;
        itemToTap.IsIntratable = true;

        base.StartTask();
    }

    private void Update()
    {
        //if (IsTaskFinished)
        //  FinishTask();
    }

    public override void FinishTask()
    {
        IsTaskFinished = true;
        //itemToTap.IsIntratable = false;
        base.FinishTask();
    }

    public override void ResetTask()
    {
        IsTaskFinished = false;
        if (resetAnimation)
        {
            itemToTap.PlayAnimation(resetAnimation.name);
        }
        base.ResetTask();
    }

    private void OnTappedOnItem(object sender, EventArgs e)
    {
        float animTime = 0;
        itemToTap.OnTapped -= OnTappedOnItem;

        if (IsTaskFinished)//closed
        {
            animTime = itemToTap.PlayAnimation(resetAnimation.name);
            itemToTap.PlayAudio(audioClip2);
            StartCoroutine(WaitAndFinish(animTime, false));
        }
        else
        {
            animTime = itemToTap.PlayAnimation(animationToPlay.name);
            itemToTap.PlayAudio(audioClip1);
            StartCoroutine(WaitAndFinish(animTime, true));
        }
    }

    private IEnumerator WaitAndFinish(float time, bool finish)
    {
        yield return new WaitForSeconds(time);
        itemToTap.OnTapped += OnTappedOnItem;
        IsTaskFinished = finish;
    }
}
