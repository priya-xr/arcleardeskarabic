﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FlipText : MonoBehaviour
{
    [HideInInspector]
    public LanguageManager languageManager;


    bool updated = false;
    Text t;

    // Start is called before the first frame update
    void Start()
    {
        t = GetComponent<Text>();
        languageManager = LanguageManager.instance;
    }

    private void Update()
    {
        if (languageManager.CurrentLaguage == Languages.Arabic)
        {
            if (updated == false)
            {
                if (t.text != "")
                    t.text = Reverse(t.text);
            }
        }

    }

    public string Reverse(string s)
    {
        Debug.Log(s);
        updated = true;
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);

    }
}
