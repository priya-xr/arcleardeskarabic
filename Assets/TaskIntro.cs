﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskIntro : TrainingTask
{
    [SerializeField]
    private TrainingMiniGame miniGame;
    [SerializeField]
    private TapItem itemToPick;
    [SerializeField]
    private float intractionTime;
    [SerializeField]
    private Button FixButton;
    [SerializeField]
    private TaskPanel taskPanel;

    public void Awake()
    {
        taskPanel.OnClickedFixButton += OnClickedFixIt;
    }

    public void OnClickedFixIt(object sender, EventArgs e)
    {
        FinishTask();
    }

    public void OnClickedFixIt()
    {
        FinishTask();
    }
    public override void StartTask()
    {
        // itemToPick.OnTapped += OnTappedOnItem;
        // itemToPick.Highlight();
        //  itemToPick.IntractionTime = intractionTime;
        // itemToPick.IsIntratable = true;
        FixButton.gameObject.SetActive(true);
        base.StartTask();
    }

    private void Update()
    {
        //if (IsTaskFinished)
        //  FinishTask();
    }

    public override void FinishTask()
    {
        FixButton.gameObject.SetActive(false);
        IsTaskFinished = true;
      //  itemToPick.IsIntratable = false;
        base.FinishTask();
    }

    public override void ResetTask()
    {
        IsTaskFinished = false;

       // itemToPick.Show();
        base.ResetTask();
    }

    //private void OnTappedOnItem(object sender, EventArgs e)
    //{
    //    itemToPick.OnTapped -= OnTappedOnItem;
    //    itemToPick.UnHighlight();

    //    //add to inventory
    //    miniGame.Inventory.AddItem(itemToPick);
    //    itemToPick.Hide();
    //    FinishTask();
    //}

    public override float CompleteTask()
    {
        itemToPick.Hide();
        return 0;
    }
}
